package App

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"qnextUserBot/Common"
	"qnextUserBot/Db"
	"qnextUserBot/Metrics"
	"qnextUserBot/QNext"
	"qnextUserBot/QNext/serialize"
	"qnextUserBot/Web"
	"syscall"
)

var postUrl string

func init() {
	fmt.Println("App:init")

	Common.ReadConf()

	var host string
	if Common.Config.Dev {
		host = "http://localhost"
	} else {
		host = Common.Config.Web.Url
	}
	postUrl = fmt.Sprintf("%s:%d/bin/userBot/request", host, Common.Config.Ab.Port)
}

func initSigterm() {
	ch := make(chan os.Signal, 2)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	<-ch

	Stop()
}

func Start() {
	fmt.Println("App:start")

	Metrics.Init()
	Db.Connect(Common.Config)
	RestartActive()
	StartEvents()

	Web.Start()

	initSigterm()
}

func RestartActive() {
	fmt.Println("RestartActive")
	var activeBots = Db.ReadActiveUserBots()
	fmt.Println("	activeBots: ", len(activeBots))
	for _, userBot := range activeBots {
		go QNext.PingUp(userBot.BotId, userBot.UserBotId, userBot.ApiId, userBot.ApiHash)
	}
}

func StartEvents() {
	go func() {
		for event := range Common.GlobalRawEvents {
			var sendBytes []byte

			var data = make(map[string]interface{})
			data["botId"] = event.BotId
			data["userBotId"] = event.UserBotId
			data["data"] = event.RawData
			data["_native"] = event.NativeData
			jsonBytes, err := json.Marshal(data)
			if err != nil {
				var errorData = make(serialize.JsonData)
				errorData["botId"] = event.BotId
				errorData["userBotId"] = event.UserBotId
				if event.RawData != nil {
					errorData["event"] = event.RawData["event"]
				}
				errorData["error"] = err.Error()
				b, _ := json.Marshal(errorData)
				sendBytes = b
			} else {
				sendBytes = jsonBytes
			}

			r := bytes.NewReader(sendBytes)
			resp, err := http.Post(postUrl, "application/json", r)
			if err != nil {
				fmt.Println("resp", resp, err)
			}
			if resp != nil && resp.Body != nil {
				if err = resp.Body.Close(); err != nil {
					fmt.Println("close err", err)
				}
			}
		}
	}()
}

func Stop() {
	fmt.Println("App:stop")
	// QNext.DestroyAll()
	Web.Close()
	Db.Close()
}
