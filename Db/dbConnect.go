package Db

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"io/ioutil"
	"qnextUserBot/Common"
)

var connectDb *sql.DB

func Connect(config *Common.ConfigData) {
	var host = config.Db.Mysql.Host
	var port = config.Db.Mysql.Port
	var name = config.Db.Mysql.Name
	var user = config.Db.Mysql.User
	var pass = config.Db.Mysql.Pass
	var cert = config.Db.Mysql.Cert

	var params = ""
	if cert != "" {
		rootCertPool := x509.NewCertPool()

		pem, err := ioutil.ReadFile(cert)
		if err != nil {
			panic(err)
		}
		if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
			panic("Failed to append PEM.")
		}

		err = mysql.RegisterTLSConfig("custom", &tls.Config{
			RootCAs: rootCertPool,
		})
		if err != nil {
			panic(err)
		}

		params = "tls=custom"
	}

	var dataSourceName = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s", user, pass, host, port, name, params)
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}

	connectDb = db

	//MongoConnect()
}

type ActiveUserBot struct {
	UserBotId int    `db:"id"`
	BotId     int    `db:"botId"`
	Title     string `db:"title"`
	ApiId     int    `db:"apiId"`
	ApiHash   string `db:"apiHash"`
}

func ReadActiveUserBots() []ActiveUserBot {
	var userBotsList []ActiveUserBot
	var sql = "SELECT id,botId,title,apiId,apiHash FROM UserBots WHERE isActive=1"
	rows, err := connectDb.Query(sql)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		userBot := ActiveUserBot{}
		err := rows.Scan(&userBot.UserBotId, &userBot.BotId, &userBot.Title, &userBot.ApiId, &userBot.ApiHash)
		if err != nil {
			fmt.Println(err)
			continue
		}
		userBotsList = append(userBotsList, userBot)
	}

	return userBotsList
}

func Close() {
	err := connectDb.Close()
	fmt.Println("Db:close", err)
}
