package Db

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"qnextUserBot/Common"
	"time"
)

/*
func NewSession(url string, ca []byte) (m *mgo.Session, err error) {
	roots := x509.NewCertPool()
	roots.AppendCertsFromPEM(ca)

	tlsConfig := &tls.Config{}
	tlsConfig.RootCAs = roots

	url = strings.TrimSuffix(url, "?ssl=true")
	dialInfo, err := mgo.ParseURL(url)
	if err != nil {
		panic(err)
	}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}

	//Here it is the session. Up to you from here ;)
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return m, err
	}

	session.SetMode(mgo.Monotonic, true)

	return session, nil
}
*/

var mongoDb *mongo.Database

func MongoConnect() {
	var cfg = &Common.Config.Db.Mongo
	var host = cfg.Host[0] + ".mdb.yandexcloud.net:27018"

	uri := fmt.Sprintf("mongodb://%s:%s@%s/%s?replicaSet=%s&ssl=true&sslCertificateAuthorityFile=%s",
		cfg.User,
		cfg.Pass,
		host,
		"qnext_userbot",
		"rs01",
		cfg.Cert,
	)

	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		panic(err)
	}

	mongoDb = client.Database("qnext_userbot")
}
