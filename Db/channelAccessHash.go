package Db

import (
	"context"
	"github.com/gotd/td/tg"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const chatAccessHashTableName = "ChatAccessHash"

type ChannelAccessHash struct {
	Id         int64
	Title      string
	AccessHash int64
}

func SaveList(chats []tg.ChatClass) {
	if mongoDb == nil {
		return
	}

	for _, chatClass := range chats {
		switch chat := chatClass.(type) {
		case *tg.Channel:
			SaveChannel(&ChannelAccessHash{
				Id:         chat.ID,
				Title:      chat.Title,
				AccessHash: chat.AccessHash,
			})
		case *tg.ChannelForbidden:
			SaveChannel(&ChannelAccessHash{
				Id:         chat.ID,
				Title:      chat.Title,
				AccessHash: chat.AccessHash,
			})
		}
	}
}

func SaveChannel(data *ChannelAccessHash) error {
	if mongoDb == nil {
		return nil
	}
	filter := bson.D{{"_id", data.Id}}
	var upsert = true
	updateOption := options.UpdateOptions{
		Upsert: &upsert,
	}
	_, err := mongoDb.Collection(chatAccessHashTableName).UpdateOne(context.TODO(), filter, data, &updateOption)
	return err
}
