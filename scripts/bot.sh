
source ~/.profile

case "$1" in
  "help")
    echo "build stop start stats restart logs dev gin port fmt"
    ;;
  "build")
    GOFLAGS=-trimpath go build -o build/qnextUserBot
    ;;
  "stop")
    sudo systemctl stop qnextuserbot.service
    ;;
  "start")
    sudo systemctl start qnextuserbot.service
    ;;
  "status")
    sudo systemctl status qnextuserbot.service
    ;;
  "restart")
    sudo systemctl restart qnextuserbot.service
    sudo journalctl -u qnextuserbot.service -f
    ;;
  "logs")
    sudo journalctl -u qnextuserbot.service -f
    ;;
  "dev")
    source ~/.profile
    GOFLAGS=-trimpath go run main.go
    ;;
  "gin")
    source ~/.profile
    gin --port 3001 --appPort=5008 run main.go
    ;;
  "port")
    sudo netstat -ltnp | grep -w ':8080'
    ;;
  "fmt")
    gofmt -s -w .
    ;;
esac