package Metrics

import (
	"flag"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

var addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

func Init() {
	flag.Parse()
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		err := http.ListenAndServe(*addr, nil)
		if err != nil {
			print(err)
		}
	}()
}
