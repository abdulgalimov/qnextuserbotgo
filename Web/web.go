package Web

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"qnextUserBot/Common"
	"qnextUserBot/Web/handlers"
	"qnextUserBot/Web/utils"
	"time"
)

var webServer *http.Server

func Start() {
	var port = Common.Config.UserBot.Port
	fmt.Println("Web:start", port)
	var portStr = fmt.Sprintf(":%d", port)

	router := mux.NewRouter()

	router.Use(parse)
	router.HandleFunc("/create", handlers.Create).Methods("POST")
	router.HandleFunc("/delete", handlers.Delete).Methods("POST")
	router.HandleFunc("/pingUp", handlers.PingUp).Methods("POST")
	router.HandleFunc("/restart", handlers.Restart).Methods("POST")
	router.HandleFunc("/stop", handlers.Stop).Methods("POST")
	router.HandleFunc("/setPhone", handlers.SetPhone).Methods("POST")
	router.HandleFunc("/setAuthCode", handlers.SetAuthCode).Methods("POST")
	router.HandleFunc("/setPassword", handlers.SetPassword).Methods("POST")
	router.HandleFunc("/getStatus", handlers.GetStatus).Methods("POST")
	router.HandleFunc("/api", handlers.Api).Methods("POST")

	server := http.Server{Addr: portStr, Handler: router}
	webServer = &server

	go func() {
		if err := server.ListenAndServe(); err != nil {
		}
	}()
}

func parse(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestData := &utils.RequestData{}
		bodyBytes, _ := ioutil.ReadAll(r.Body)
		err := json.Unmarshal(bodyBytes, requestData)
		if err != nil {
			utils.RespondError(w, err)
			return
		}

		var bodyJson = string(bodyBytes)
		fmt.Println("Web:request", r.RequestURI, bodyJson)

		ctx := context.WithValue(r.Context(), "requestData", *requestData)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func Close() {
	fmt.Println("Web:close")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := webServer.Shutdown(ctx); err != nil {
		fmt.Println("Close err", err)
	}
}
