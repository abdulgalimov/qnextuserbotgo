package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func SetAuthCode(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	authCode, err := requestData.GetString(authCodeKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, authCodeKey, err)
		return
	}

	result := QNext.SetCode(requestData.UserBotId, authCode)
	if !result.Ok {
		utils.RespondQNextError(w, result.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
