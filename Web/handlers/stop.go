package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func Stop(w http.ResponseWriter, r *http.Request) {

	requestData := r.Context().Value(requestDataKey).(utils.RequestData)

	var createResult = QNext.Stop(requestData.UserBotId)
	if !createResult.Ok {
		utils.RespondQNextError(w, createResult.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
