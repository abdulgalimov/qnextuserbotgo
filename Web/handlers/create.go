package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func Create(w http.ResponseWriter, r *http.Request) {

	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	apiId, err := requestData.GetInt(apiIdKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, apiIdKey, err)
		return
	}
	apiHash, err := requestData.GetString(apiHashKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, apiHashKey, err)
		return
	}

	var createResult = QNext.Create(requestData.BotId, requestData.UserBotId, apiId, apiHash)
	if !createResult.Ok {
		utils.RespondQNextError(w, createResult.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
