package handlers

const (
	requestDataKey string = "requestData"
	methodKey      string = "method"
	apiIdKey       string = "apiId"
	apiHashKey     string = "apiHash"
	phoneKey       string = "phone"
	authCodeKey    string = "authCode"
	passwordKey    string = "password"
)
