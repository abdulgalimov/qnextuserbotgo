package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func Api(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	method, err := requestData.GetString(methodKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, methodKey, err)
		return
	}

	result := QNext.CallApi(requestData.UserBotId, method, requestData)
	if !result.Ok {
		utils.RespondQNextResultError(w, result)
		return
	}

	utils.RespondDataOk(w, result.Data, result.NativeData)
}
