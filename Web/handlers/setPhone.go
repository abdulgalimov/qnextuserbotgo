package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func SetPhone(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	phone, err := requestData.GetString(phoneKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, phoneKey, err)
		return
	}

	result := QNext.SetPhone(requestData.UserBotId, phone)
	if !result.Ok {
		utils.RespondQNextError(w, result.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
