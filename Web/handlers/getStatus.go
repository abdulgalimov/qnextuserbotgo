package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func GetStatus(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)

	var result, status = QNext.GetStatus(requestData.UserBotId)
	if !result.Ok {
		utils.RespondQNextError(w, result.ErrorMessage)
		return
	}

	var resp = make(map[string]interface{})
	resp["ok"] = true
	resp["status"] = status

	utils.Respond(w, resp)
}
