package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func SetPassword(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	password, err := requestData.GetString(passwordKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, passwordKey, err)
		return
	}

	result := QNext.SetPassword(requestData.UserBotId, password)
	if !result.Ok {
		utils.RespondQNextError(w, result.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
