package handlers

import (
	"net/http"
	"qnextUserBot/QNext"
	"qnextUserBot/Web/utils"
)

func Restart(w http.ResponseWriter, r *http.Request) {
	requestData := r.Context().Value(requestDataKey).(utils.RequestData)
	apiId, err := requestData.GetInt(apiIdKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, apiIdKey, err)
		return
	}
	apiHash, err := requestData.GetString(apiHashKey)
	if err != nil {
		utils.RespondErrorParameterInvalid(w, apiHashKey, err)
		return
	}

	var result = QNext.Restart(requestData.BotId, requestData.UserBotId, apiId, apiHash)
	if !result.Ok {
		utils.RespondQNextError(w, result.ErrorMessage)
		return
	}

	utils.RespondOk(w)
}
