package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"qnextUserBot/Common"
)

type RequestData struct {
	BotId      int                    `json:"botId"`
	UserBotId  int                    `json:"userBotId"`
	Common.DataContainer
}

func Message(status bool, message string) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}

func Respond(w http.ResponseWriter, data map[string]interface{}) {
	fmt.Println("Web:respond", data)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func RespondError(w http.ResponseWriter, err error) {
	var resp = make(map[string]interface{})
	resp["ok"] = false
	resp["errorMessage"] = fmt.Sprintf("%s", err.Error())
	Respond(w, resp)
}

func RespondErrorParameterInvalid(w http.ResponseWriter, parameter string, err error) {
	var resp = make(map[string]interface{})
	resp["ok"] = false
	resp["errorCode"] = fmt.Sprintf("%s", Common.InvalidParameter)
	resp["errorMessage"] = fmt.Sprintf("%s; parameter name: %s", err.Error(), parameter)
	Respond(w, resp)
}

func RespondErrorText(w http.ResponseWriter, message string) {
	var resp = make(map[string]interface{})
	resp["ok"] = false
	resp["errorMessage"] = message
	Respond(w, resp)
}

func RespondQNextError(w http.ResponseWriter, message Common.ResultErrorMessages) {
	var resp = make(map[string]interface{})
	resp["ok"] = false
	resp["errorCode"] = fmt.Sprintf("%s", message)
	Respond(w, resp)
}

func RespondQNextResultError(w http.ResponseWriter, result *Common.Result) {
	var resp = make(map[string]interface{})
	resp["ok"] = false
	resp["errorCode"] = fmt.Sprintf("%s", result.ErrorMessage)
	if result.Error != nil {
		resp["errorMessage"] = fmt.Sprintf("%s", result.Error.Error())
	}
	Respond(w, resp)
}

func RespondOk(w http.ResponseWriter) {
	var resp = make(map[string]interface{})
	resp["ok"] = true
	Respond(w, resp)
}

func RespondDataOk(w http.ResponseWriter, data map[string]interface{}, nativeData interface{}) {
	var resp = make(map[string]interface{})
	resp["ok"] = true
	resp["data"] = data
	resp["_native"] = nativeData
	Respond(w, resp)
}
