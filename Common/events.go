package Common

type RawEvent struct {
	BotId      int
	UserBotId  int
	RawData    map[string]interface{}
	NativeData interface{}
}

var GlobalRawEvents = make(chan *RawEvent, 10)
