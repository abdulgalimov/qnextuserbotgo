package Common

import (
	"encoding/base64"
	"fmt"
	"math/rand"
	"reflect"
	"strconv"
	"time"
)

var randomSource = rand.NewSource(time.Now().UnixNano())
var Random = rand.New(randomSource)

type DataReader interface {
	GetReader(key string) (DataReader, error)
	GetString(key string) (string, error)
	GetFloat64(key string) (float64, error)
	GetInt64(key string) (int64, error)
	GetInt(key string) (int, error)
	GetIntList(key string) ([]int, error)
	GetInt64List(key string) ([]int64, error)
	GetBool(key string) bool
	GetChatId(key string) (int64, error)
	GetChannelId(key string) (int64, error)
	GetFileReference(key string) ([]byte, error)
}

type DataContainer struct {
	DataSource map[string]interface{} `json:"data"`
	DataReader
}

func (r *DataContainer) GetReader(key string) (DataReader, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return nil, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case map[string]interface{}:
		return &DataContainer{
			DataSource: value,
		}, nil
	}
	return nil, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetString(key string) (string, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return "", ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch valueStr := valueClass.(type) {
	case string:
		return valueStr, nil
	}
	return "", ParameterTypeInvalidError{
		Parameter: key,
	}
}
func (r *DataContainer) GetFloat64(key string) (float64, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return 0, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case float64:
		return value, nil
	}
	return 0, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetIntList(key string) ([]int, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return nil, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case []int:
		return value, nil
	case []int64:
		var newList = make([]int, len(value))
		for index, item := range value {
			newList[index] = int(item)
		}
		return newList, nil
	case []float64:
		var newList = make([]int, len(value))
		for index, item := range value {
			newList[index] = int(item)
		}
		return newList, nil
	case []float32:
		var newList = make([]int, len(value))
		for index, item := range value {
			newList[index] = int(item)
		}
		return newList, nil
	case []interface{}:
		var newList = make([]int, len(value))
		for index, item := range value {
			intValue, err := _interfaceToInt(item)
			if err == nil {
				newList[index] = intValue
			}
		}
		return newList, nil
	default:
		fmt.Println("GetIntList def", valueClass, reflect.TypeOf(value), fmt.Sprintf("%T", value))
	}
	return nil, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetInt64List(key string) ([]int64, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return nil, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case []int:
		var newList = make([]int64, len(value))
		for index, item := range value {
			newList[index] = int64(item)
		}
		return newList, nil
	case []int64:
		return value, nil
	case []float64:
		var newList = make([]int64, len(value))
		for index, item := range value {
			newList[index] = int64(item)
		}
		return newList, nil
	case []float32:
		var newList = make([]int64, len(value))
		for index, item := range value {
			newList[index] = int64(item)
		}
		return newList, nil
	case []interface{}:
		var newList = make([]int64, len(value))
		for index, item := range value {
			intValue, err := _interfaceToInt64(item)
			if err == nil {
				newList[index] = intValue
			}
		}
		return newList, nil
	default:
		fmt.Println("GetIntList def", valueClass, reflect.TypeOf(value), fmt.Sprintf("%T", value))
	}
	return nil, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetInt(key string) (int, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return 0, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case int:
		return value, nil
	case float32:
		return int(value), nil
	case float64:
		return int(value), nil
	}
	return 0, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetInt64(key string) (int64, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return 0, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case int64:
		return value, nil
	case int:
		return int64(value), nil
	case int32:
		return int64(value), nil
	case float64:
		return int64(value), nil
	case float32:
		return int64(value), nil
	case string:
		value64, err2 := strconv.ParseInt(value, 10, 64)
		if err2 != nil {
			return 0, err2
		}
		return value64, nil
	}
	return 0, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func (r *DataContainer) GetBool(key string) bool {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return false
	}
	switch value := valueClass.(type) {
	case string:
		return value == "1"
	case int64:
		return value > 0
	case int:
		return value > 0
	case float64:
		return value > 0
	case bool:
		return value
	case map[string]interface{}:
		return true
	case []interface{}:
		return true
	}

	return false
}

func (r *DataContainer) GetFileReference(key string) ([]byte, error) {
	valueClass, ok := r.DataSource[key]
	if !ok {
		return nil, ParameterNotFoundError{
			Parameter: key,
		}
	}
	switch value := valueClass.(type) {
	case string:
		return base64.StdEncoding.DecodeString(value)
	}

	return nil, ParameterTypeInvalidError{
		Parameter: key,
	}
}

func _interfaceToInt(valueClass interface{}) (int, error) {
	switch value := valueClass.(type) {
	case int:
		return value, nil
	case int64:
		return int(value), nil
	case float32:
		return int(value), nil
	case float64:
		return int(value), nil
	}
	return 0, ParameterTypeInvalidError{}
}

func _interfaceToInt64(valueClass interface{}) (int64, error) {
	switch value := valueClass.(type) {
	case int:
		return int64(value), nil
	case int64:
		return value, nil
	case float32:
		return int64(value), nil
	case float64:
		return int64(value), nil
	}
	return 0, ParameterTypeInvalidError{}
}

func (r *DataContainer) GetChatId(key string) (int64, error) {
	id, err := r.GetInt64(key)
	if err != nil {
		return 0, err
	}
	return ConvertChatIdToUser(id), nil
}

func (r *DataContainer) GetChannelId(key string) (int64, error) {
	id, err := r.GetInt64(key)
	if err != nil {
		return 0, err
	}
	return ConvertChannelIdToUser(id), nil
}
