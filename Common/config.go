package Common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

type ConfigData struct {
	App     AppConfig
	Dev     bool              `json:"dev"`
	Db      DbConfigData      `json:"db"`
	UserBot UserBotConfigData `json:"userBot"`
	Web     WebConfigData     `json:"web"`
	Ab      AbConfigData      `json:"ab"`
}
type AppConfig struct {
	DatabaseDir string
}
type WebConfigData struct {
	Url string `json:"url"`
}
type AbConfigData struct {
	Port int `json:"port"`
}
type DbConfigData struct {
	Mysql MysqlConfigData `json:"mysql"`
	Mongo MongoConfigData `json:"mongo"`
}
type MysqlConfigData struct {
	Host string `json:"host"`
	Port int    `json:"port"`
	Name string `json:"name"`
	User string `json:"user"`
	Pass string `json:"pass"`
	Cert string `json:"crt"`
}
type MongoConfigData struct {
	Name string   `json:"name"`
	User string   `json:"user"`
	Pass string   `json:"pass"`
	Host []string `json:"hosts"`
	Cert string   `json:"crt"`
}
type UserBotConfigData struct {
	Port    int    `json:"port"`
	ApiId   int    `json:"apiId"`
	ApiHash string `json:"apiHash"`
	Logger  bool   `json:"logger"`
}

var Config *ConfigData

func ReadConf() {
	fmt.Println("Read Config")
	configFile, configDir := getConfigFile()
	if configFile == "" {
		panic("Not found config file")
	}
	jsonFile, err := os.Open(configFile)
	if err != nil {
		fmt.Println(err)
		panic("Invalid config file")
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config = ConfigData{}
	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		panic(err)
	}

	Config = &config
	Config.App.DatabaseDir = configDir

	fmt.Println("	config file", configFile)
	fmt.Println("	database dir", Config.App.DatabaseDir)
}

const configFileName = "config.json"

func getConfigFile() (string, string) {
	currentDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	var configFile = currentDir + "/" + configFileName
	if _, err := os.Stat(configFile); !os.IsNotExist(err) {
		return configFile, currentDir
	}

	pwdDir, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	configFile = pwdDir + "/" + configFileName
	if _, err := os.Stat(configFile); !os.IsNotExist(err) {
		return configFile, pwdDir
	}

	var buildDir = pwdDir + "/build"
	configFile = buildDir + "/" + configFileName
	if _, err := os.Stat(configFile); !os.IsNotExist(err) {
		return configFile, buildDir
	}

	if _, err := os.Stat(configFileName); !os.IsNotExist(err) {
		return configFileName, "."
	}
	return "", ""
}
