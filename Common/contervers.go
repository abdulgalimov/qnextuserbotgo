package Common

import (
	"math"
	"strconv"
)

func ConvertChannelIdToBot(channelId int64) int64 {
	var str = strconv.FormatInt(channelId, 10)
	var count = len(str)
	return -(100*int64(math.Pow(10, float64(count))) + channelId)
}

func ConvertChannelIdToUser(channelId int64) int64 {
	if channelId < 0 {
		channelId *= -1
	}
	var str = strconv.FormatInt(channelId, 10)
	str = str[3:]
	v, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return v
}

func ConvertChatIdToUser(chatId int64) int64 {
	if chatId < 0 {
		return -chatId
	}
	return chatId
}

func ConvertChatIdToBot(chatId int64) int64 {
	if chatId > 0 {
		return -chatId
	}
	return chatId
}
