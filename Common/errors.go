package Common

import "fmt"

type ParameterTypeInvalidError struct {
	Parameter string
}

func (r ParameterTypeInvalidError) Error() string {
	return fmt.Sprintf("TypeInvalidError: parameter (%s) not found", r.Parameter)
}

type ParameterNotFoundError struct {
	Parameter string
}

func (r ParameterNotFoundError) Error() string {
	return fmt.Sprintf("ParameterNotFoundError: parameter (%s) not found", r.Parameter)
}
