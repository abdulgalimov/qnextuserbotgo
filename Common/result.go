package Common

type Result struct {
	Ok           bool
	ErrorMessage ResultErrorMessages
	Data         map[string]interface{}
	NativeData   interface{}
	Error        error
}

type ResultErrorMessages string

const (
	CreateDuplicate  ResultErrorMessages = "QNextCreateDuplicate"
	BotInvalidStatus ResultErrorMessages = "BotInvalidStatus"
	BotNotFound      ResultErrorMessages = "BotNotFound"
	UndefinedError   ResultErrorMessages = "UndefinedError"
	ApiError         ResultErrorMessages = "ApiError"
	InvalidParameter ResultErrorMessages = "InvalidParameter"
	FileError 		 ResultErrorMessages = "FileError"
)
