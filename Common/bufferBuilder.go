package Common

import (
	"bytes"
	"encoding/binary"
)

const (
	TypeInt    uint16 = 1
	TypeString uint16 = 2
)

type BufferBuilder struct {
	buffer bytes.Buffer
}

func (b *BufferBuilder) Init() {
	b.buffer = bytes.Buffer{}
}

func (b *BufferBuilder) AddUint32(value uint32) {
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs, value)
	b.buffer.Write(bs)
}
func (b *BufferBuilder) AddUint16(value uint16) {
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint16(bs, value)
	b.buffer.Write(bs)
}

func (b *BufferBuilder) AddInt(value int) {
	b.AddUint32(uint32(value))
}

func (b *BufferBuilder) AddString(value string) {
	b.AddInt(len(value))
	b.buffer.WriteString(value)
}
func (b *BufferBuilder) AddKeyInt(key string, value int) {
	b.AddString(key)
	b.AddUint16(TypeInt)
	b.AddUint32(uint32(value))
}
func (b *BufferBuilder) AddKeyString(key string, value string) {
	b.AddString(key)
	b.AddUint16(TypeString)
	b.AddString(value)
}

func (b *BufferBuilder) GetBytes() []byte {
	return b.buffer.Bytes()
}
