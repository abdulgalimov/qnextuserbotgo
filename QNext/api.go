package QNext

import (
	"context"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/api"
	"qnextUserBot/Web/utils"
	"time"
)

func CallApi(userBotId int, method string, data utils.RequestData) *Common.Result {
	var userBot = userBots[userBotId]
	if userBot == nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotNotFound,
		}
	}

	timeoutCtx, cancelFunc := context.WithTimeout(context.Background(), time.Second*1000)
	defer cancelFunc()

	var requestContext = api.RequestContext{
		Raw:     userBot.Raw,
		Context: timeoutCtx,
		Data:    &data,
	}

	switch method {
	case "getMe":
		return api.GetMe(&requestContext)

	case "messages.getAllChats":
		return api.MessagesGetAllChats(&requestContext)
	case "messages.getChats":
		return api.MessagesGetChats(&requestContext)
	case "messages.getCommonChats":
		return api.MessagesGetCommonChats(&requestContext)
	case "messages.sendMessage":
		fallthrough
	case "sendMessage":
		return api.MessagesSendMessage(&requestContext)
	case "messages.getFullChat":
		return api.MessagesGetFullChat(&requestContext)
	case "messages.reportSpam":
		return api.MessagesReportSpam(&requestContext)
	case "messages.forwardMessages":
		return api.MessagesForwardMessages(&requestContext)
	case "messages.getOnlines":
		return api.MessagesGetOnlines(&requestContext)
	case "messages.getMessagesReactions":
		return api.MessagesGetMessagesReactions(&requestContext)
	case "messages.getMessageReactionsList":
		return api.MessagesGetMessageReactionsList(&requestContext)

	case "messages.getStickerSet":
		return api.MessagesGetStickerSet(&requestContext)

	case "stats.getMegagroupStats":
		return api.StatsGetMegagroupStats(&requestContext)
	case "stats.getBroadcastStats":
		return api.StatsGetBroadcastStats(&requestContext)
	case "stats.loadAsyncGraph":
		return api.StatsLoadAsyncGraph(&requestContext)

	case "phone.getGroupCall":
		fallthrough
	case "getGroupCall":
		return api.GetGroupCall(&requestContext)
	case "phone.toggleGroupCallRecord":
		fallthrough
	case "groupCallRecord":
		return api.GroupCallRecord(&requestContext)

	case "channels.joinChannel":
		fallthrough
	case "joinChannel":
		return api.JoinChannel(&requestContext)
	case "channels.leaveChannel":
		return api.LeaveChannel(&requestContext)
	case "channels.getFullChannel":
		return api.ChannelsGetFullChannel(&requestContext)
	case "channels.toggleSlowMode":
		return api.ToggleSlowMode(&requestContext)
	case "channels.deleteMessages":
		return api.ChannelsDeleteMessages(&requestContext)

	case "upload.getFile":
		return api.UploadGetFile(&requestContext)
	}
	return &Common.Result{Ok: false}
}
