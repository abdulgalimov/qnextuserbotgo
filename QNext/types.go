package QNext

import (
	"context"
	"fmt"
	"github.com/gotd/td/telegram"
	"github.com/gotd/td/telegram/auth"
	"github.com/gotd/td/telegram/message"
	"github.com/gotd/td/tg"
)

type UserBot struct {
	id          int
	client      *telegram.Client
	Context     context.Context
	cancel      context.CancelFunc
	cfg         *tg.Config
	appConfig   tg.JSONValueClass
	readValues  *ReadValue
	status      AuthStatus
	updateFlags UpdateFlags

	Raw    *tg.Client
	sender *message.Sender

	auth.UserAuthenticator
}

func (ub *UserBot) log(params ...interface{}) {
	fmt.Printf("bot.%d %v\n", ub.id, params)
}

func (ub *UserBot) SetAuthStatus(status AuthStatus) {
	ub.log("SetAuthStatus", status)
	ub.status = status
}

func (ub *UserBot) GetSender() *message.Sender {
	if ub.sender == nil {
		ub.sender = message.NewSender(ub.Raw)
	}
	return ub.sender
}

func (ub *UserBot) Phone(ctx context.Context) (string, error) {
	ub.log("NeedPhone")
	ub.SetAuthStatus(NeedPhoneStatus)
	phone := ub.readValues.getValue()
	ub.SetAuthStatus(ReceivedPhoneStatus)
	return phone.(string), nil
}
func (ub *UserBot) Password(ctx context.Context) (string, error) {
	ub.log("NeedPassword")
	ub.SetAuthStatus(NeedPasswordStatus)
	pass := ub.readValues.getValue()
	ub.SetAuthStatus(ReceivedPasswordStatus)
	return pass.(string), nil
}
func (ub *UserBot) Code(ctx context.Context, sentCode *tg.AuthSentCode) (string, error) {
	ub.log("NeedCode")
	ub.SetAuthStatus(NeedAuthCodeStatus)
	code := ub.readValues.getValue()
	ub.SetAuthStatus(ReceivedAuthCodeStatus)
	return code.(string), nil
}
func (ub *UserBot) AcceptTermsOfService(ctx context.Context, tos tg.HelpTermsOfService) error {
	ub.log("NeedAcceptTermsOfService")
	ub.SetAuthStatus(NeedAcceptTerms)
	ub.readValues.getValue()
	ub.SetAuthStatus(AcceptedTerms)
	return nil
}
func (ub *UserBot) SignUp(ctx context.Context) (auth.UserInfo, error) {
	ub.log("NeedSignUp")
	return auth.UserInfo{}, nil
}

type ReadValue struct {
	userBotId int
	name      string
	channel   chan interface{}
	isClosed  bool
}

func CreateReadValue(userBotId int, name string) *ReadValue {
	return &ReadValue{
		userBotId: userBotId,
		name:      name,
		channel:   make(chan interface{}, 10),
		isClosed:  false,
	}
}
func (r *ReadValue) getValue() interface{} {
	if r.isClosed {
		fmt.Println("get in closed value", r.userBotId, r.name)
		return ""
	}
	value := <-r.channel
	return value
}
func (r *ReadValue) setValue(value interface{}) {
	if r.isClosed {
		fmt.Println("set in closed value", r.userBotId, r.name)
		return
	}
	r.channel <- value
}
func (r *ReadValue) close() {
	if !r.isClosed {
		close(r.channel)
		r.isClosed = true
	}
}

type UpdateFlags struct {
	Flags1 Bitmask
}

func (f *UpdateFlags) HasFlag1(flag Bitmask) bool {
	return f.Flags1 == 0 || f.Flags1.HasFlag(flag)
}

type Bitmask uint32

func (f Bitmask) HasFlag(flag Bitmask) bool { return f&flag != 0 }
func (f *Bitmask) AddFlag(flag Bitmask)     { *f |= flag }
func (f *Bitmask) ClearFlag(flag Bitmask)   { *f &= ^flag }
func (f *Bitmask) ToggleFlag(flag Bitmask)  { *f ^= flag }

type UserBotUpdateFlags int

const (
	UpdateUndefinedFlag1 Bitmask = 1 << iota
	UpdateNewMessageFlag1
	UpdateNewChannelMessageFlag1
	UpdateUserStatusFlag1
	UpdateUserPhoneFlag1
	UpdateUserNameFlag1
	UpdateDeleteMessagesFlag1
	UpdateDeleteChannelMessagesFlag1
	UpdateUserTypingFlag1
	UpdateChannelMessageViewsFlag1
	UpdateChannelParticipantFlag1
	UpdateGroupCallFlag1
	UpdateGroupCallParticipantsFlag1
	UpdateChatUserTypingFlag1
	UpdateChatParticipantsFlag1
	UpdateChatParticipantFlag1
	UpdateChatParticipantAddFlag1
	UpdateChatParticipantDeleteFlag1
	UpdateChatParticipantAdminFlag1
	UpdatePhoneCallFlag1
	UpdateChannelFlag1
	UpdateChatFlag1
)
