package api

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/deserialize"
)

func UploadGetFile(requestContext *RequestContext) *Common.Result {
	fmt.Println("UploadGetFile")
	location, errLocation := deserialize.GetInputFileLocation(requestContext.Data)
	if errLocation != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errLocation,
		}
	}

	var isFinish = false
	var offset = 0
	bytes := make([]byte, 0)
	var loopCount = 0
	var mtime = 0
	var fileTypeStr = ""
	for !isFinish && loopCount < 5 {
		loopCount++
		request := tg.UploadGetFileRequest{
			Precise: true,
			Location: location,
			Offset: offset,
			Limit: 1024*1024,
			CDNSupported: false,
		}
		uploadFileClass, errApi := requestContext.Raw.UploadGetFile(requestContext.Context, &request)
		if errApi != nil {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        errApi,
			}
		}
		isFinish = true
		switch uploadFile := uploadFileClass.(type) {
		case *tg.UploadFile:
			fileTypeClass := uploadFile.Type
			fileTypeStr = fileTypeClass.TypeName()
			var count = len(uploadFile.Bytes)
			bytes = append(bytes, uploadFile.Bytes...)
			mtime = uploadFile.Mtime
			offset += count
			switch fileTypeClass.(type) {
			case *tg.StorageFilePartial:
				if count == request.Limit {
					isFinish = false
				}
			}
		case *tg.UploadFileCDNRedirect:
			return &Common.Result{
				Ok:				false,
				ErrorMessage: 	Common.FileError,
				Error: 			errors.New("unsupported cdn file"),
			}
		}
	}

	if !isFinish {
		return &Common.Result{
			Ok:				false,
			ErrorMessage: 	Common.FileError,
			Error: 			errors.New("file is too long, max 5mb"),
		}
	}

	var data = make(map[string]interface{})
	data["base64"] = base64.StdEncoding.EncodeToString(bytes)
	data["mtime"] = mtime
	data["type"] = fileTypeStr
	return &Common.Result{
		Ok:			true,
		NativeData: nil,
		Data: data,
	}
}