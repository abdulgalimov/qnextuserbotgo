package api

import (
	"context"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

type RequestContext struct {
	Raw     *tg.Client
	Context context.Context
	Data    Common.DataReader
}
