package api

import (
	"fmt"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/deserialize"
	"qnextUserBot/QNext/serialize"
)

func StatsGetMegagroupStats(requestContext *RequestContext) *Common.Result {
	channel, err := deserialize.GetInputChannelClass(requestContext.Data)
	fmt.Println("peer", channel, err)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	dark := requestContext.Data.GetBool("dark")

	request := tg.StatsGetMegagroupStatsRequest{
		Channel: channel,
		Dark:    dark,
	}
	result, errApi := requestContext.Raw.StatsGetMegagroupStats(requestContext.Context, &request)
	if errApi != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errApi,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.StatsMegagroupStats(result),
		NativeData: result,
	}
}

func StatsGetBroadcastStats(requestContext *RequestContext) *Common.Result {
	channel, err := deserialize.GetInputChannelClass(requestContext.Data)
	fmt.Println("peer", channel, err)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	dark := requestContext.Data.GetBool("dark")

	request := tg.StatsGetBroadcastStatsRequest{
		Channel: channel,
		Dark:    dark,
	}
	result, errApi := requestContext.Raw.StatsGetBroadcastStats(requestContext.Context, &request)
	if errApi != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errApi,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.StatsBroadcastStats(result),
		NativeData: result,
	}
}

func StatsLoadAsyncGraph(requestContext *RequestContext) *Common.Result {
	token, errToken := requestContext.Data.GetString("token")
	if errToken != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errToken,
		}
	}

	x, errX := requestContext.Data.GetInt64("x")
	if errX != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errX,
		}
	}

	request := tg.StatsLoadAsyncGraphRequest{
		Token: token,
		X:     x,
	}
	result, errApi := requestContext.Raw.StatsLoadAsyncGraph(requestContext.Context, &request)
	if errApi != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errApi,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.StatsGraphClass(result),
		NativeData: result,
	}
}
