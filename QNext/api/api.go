package api

import (
	"github.com/gotd/td/telegram/message"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/deserialize"
	"qnextUserBot/QNext/serialize"
)

func GetMe(requestContext *RequestContext) *Common.Result {
	var id = tg.InputUserSelf{}
	usersFull, err := requestContext.Raw.UsersGetFullUser(requestContext.Context, &id)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	var userFullData = serialize.UserFull(&usersFull.FullUser)
	return &Common.Result{
		Ok:   true,
		Data: userFullData,
	}
}

func JoinChannel(requestContext *RequestContext) *Common.Result {
	if resolve, err := requestContext.Data.GetString("resolve"); err == nil {
		var sender = message.NewSender(requestContext.Raw)
		updateClass, err := sender.Resolve(resolve).Join(requestContext.Context)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	if link, err := requestContext.Data.GetString("link"); err == nil {
		var sender = message.NewSender(requestContext.Raw)
		updateClass, err := sender.JoinLink(requestContext.Context, link)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	if hash, err := requestContext.Data.GetString("hash"); err == nil {
		var sender = message.NewSender(requestContext.Raw)
		updateClass, err := sender.JoinHash(requestContext.Context, hash)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	inputChannel, errInput := deserialize.GetInputChannelClass(requestContext.Data)
	if errInput == nil {
		updateClass, err := requestContext.Raw.ChannelsJoinChannel(requestContext.Context, inputChannel)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	return &Common.Result{
		Ok:           false,
		ErrorMessage: Common.ApiError,
	}
}

func LeaveChannel(requestContext *RequestContext) *Common.Result {
	if resolve, err := requestContext.Data.GetString("resolve"); err == nil {
		var sender = message.NewSender(requestContext.Raw)
		updateClass, err := sender.Resolve(resolve).Leave(requestContext.Context)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	inputChannel, errInput := deserialize.GetInputChannelClass(requestContext.Data)
	if errInput == nil {
		updateClass, err := requestContext.Raw.ChannelsLeaveChannel(requestContext.Context, inputChannel)
		if err == nil {
			return &Common.Result{
				Ok:         true,
				Data:       serialize.UpdatesClass(updateClass),
				NativeData: updateClass,
			}
		} else {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.ApiError,
				Error:        err,
			}
		}
	}

	return &Common.Result{
		Ok:           false,
		ErrorMessage: Common.ApiError,
	}
}

func GetGroupCall(requestContext *RequestContext) *Common.Result {
	chatId, chatIdErr := requestContext.Data.GetInt64("chatId")
	accessHash, accessHashErr := requestContext.Data.GetInt64("accessHash")
	if chatIdErr != nil || accessHashErr != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
		}
	}
	limit, limitErr := requestContext.Data.GetInt("limit")
	if limitErr != nil {
		limit = 1
	}

	var inputGroupCall = tg.InputGroupCall{
		ID:         chatId,
		AccessHash: accessHash,
	}
	res, err := requestContext.Raw.PhoneGetGroupCall(requestContext.Context, &tg.PhoneGetGroupCallRequest{
		Call:  inputGroupCall,
		Limit: limit,
	})
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}

	return &Common.Result{
		Ok:         true,
		NativeData: res,
	}
}

func GroupCallRecord(requestContext *RequestContext) *Common.Result {
	start := requestContext.Data.GetBool("start")
	title, _ := requestContext.Data.GetString("title")

	callId, chatIdErr := requestContext.Data.GetInt64("chatId")
	accessHash, accessHashErr := requestContext.Data.GetInt64("accessHash")
	if chatIdErr != nil || accessHashErr != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
		}
	}
	var call = tg.InputGroupCall{
		ID:         callId,
		AccessHash: accessHash,
	}
	res, err := requestContext.Raw.PhoneToggleGroupCallRecord(requestContext.Context, &tg.PhoneToggleGroupCallRecordRequest{
		Start: start,
		Call:  call,
		Title: title,
	})
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}

	return &Common.Result{
		Ok:         true,
		NativeData: res,
	}
}

func ToggleSlowMode(requestContext *RequestContext) *Common.Result {
	seconds, secondsErr := requestContext.Data.GetInt("seconds")
	if secondsErr != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        secondsErr,
		}
	}

	inputChannel, inputChannelErr := deserialize.GetInputChannelClass(requestContext.Data)
	if inputChannelErr != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        inputChannelErr,
		}
	}

	updatesResult, err := requestContext.Raw.ChannelsToggleSlowMode(requestContext.Context, &tg.ChannelsToggleSlowModeRequest{
		Channel: inputChannel,
		Seconds: seconds,
	})
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	return &Common.Result{
		Ok:         true,
		NativeData: updatesResult,
		Data:       serialize.UpdatesClass(updatesResult),
	}
}
