package api

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/deserialize"
	"qnextUserBot/QNext/serialize"
)

func ChannelsGetFullChannel(requestContext *RequestContext) *Common.Result {
	inputChannel, errChannel := deserialize.GetInputChannelClass(requestContext.Data)
	if errChannel != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errChannel,
		}
	}
	messagesChatFull, err := requestContext.Raw.ChannelsGetFullChannel(requestContext.Context, inputChannel)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.MessagesChatFull(messagesChatFull),
		NativeData: messagesChatFull,
	}
}

func ChannelsDeleteMessages(requestContext *RequestContext) *Common.Result {
	inputChannel, errChannel := deserialize.GetInputChannelClass(requestContext.Data)
	if errChannel != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errChannel,
		}
	}

	ids, errIdList := requestContext.Data.GetIntList("id")
	if errIdList != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errIdList,
		}
	}

	request := tg.ChannelsDeleteMessagesRequest{
		Channel: inputChannel,
		ID:      ids,
	}

	result, err := requestContext.Raw.ChannelsDeleteMessages(requestContext.Context, &request)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	return &Common.Result{
		Ok:         true,
		NativeData: result,
	}
}
