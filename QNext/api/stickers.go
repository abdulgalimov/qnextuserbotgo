package api

import (
	"qnextUserBot/Common"
	"qnextUserBot/QNext/deserialize"
	"qnextUserBot/QNext/serialize"
)

func MessagesGetStickerSet(requestContext *RequestContext) *Common.Result {
	inputStickerSetRequest, errSticker := deserialize.GetMessagesGetStickerSetRequest(requestContext.Data)
	if errSticker != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errSticker,
		}
	}

	result, errApi := requestContext.Raw.MessagesGetStickerSet(requestContext.Context, inputStickerSetRequest)
	if errApi != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errApi,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.MessagesStickerSetClass(result),
		NativeData: result,
	}
}
