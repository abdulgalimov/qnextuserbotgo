package api

import (
	"fmt"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/Db"
	"qnextUserBot/QNext/deserialize"
	"qnextUserBot/QNext/serialize"
)

func MessagesSendMessage(requestContext *RequestContext) *Common.Result {
	peer, err := deserialize.GetInputPeer(requestContext.Data, "peer")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	text, err := requestContext.Data.GetString("text")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}

	silent := requestContext.Data.GetBool("silent")
	noWebPage := requestContext.Data.GetBool("noWebPage")
	background := requestContext.Data.GetBool("background")
	clearDraft := requestContext.Data.GetBool("clearDraft")
	var request = tg.MessagesSendMessageRequest{
		Peer:       peer,
		Message:    text,
		RandomID:   Common.Random.Int63(),
		NoWebpage:  noWebPage,
		Silent:     silent,
		Background: background,
		ClearDraft: clearDraft,
	}

	scheduleDate, errScheduleDate := requestContext.Data.GetInt("scheduleDate")
	if errScheduleDate == nil {
		request.ScheduleDate = scheduleDate
	}
	replyToMessageId, errReplyToMessageId := requestContext.Data.GetInt("replyToMessageId")
	if errReplyToMessageId == nil {
		request.ReplyToMsgID = replyToMessageId
	}

	updateClass, err := requestContext.Raw.MessagesSendMessage(requestContext.Context, &request)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}

	return &Common.Result{
		Ok:         true,
		Data:       serialize.UpdatesClass(updateClass),
		NativeData: updateClass,
	}
}

func MessagesForwardMessages(requestContext *RequestContext) *Common.Result {
	fromPeer, errFromPeer := deserialize.GetInputPeer(requestContext.Data, "fromPeer")
	if errFromPeer != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errFromPeer,
		}
	}
	toPeer, errToPeer := deserialize.GetInputPeer(requestContext.Data, "toPeer")
	if errToPeer != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errToPeer,
		}
	}
	var ids []int
	if id, errId := requestContext.Data.GetInt("id"); errId == nil {
		ids = []int{id}
	} else if idList, errIdList := requestContext.Data.GetIntList("id"); errIdList == nil {
		ids = idList
	} else {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        Common.ParameterTypeInvalidError{Parameter: "id"},
		}
	}

	silent := requestContext.Data.GetBool("silent")
	background := requestContext.Data.GetBool("background")
	withMyScope := requestContext.Data.GetBool("withMyScope")
	scheduleDate, _ := requestContext.Data.GetInt("scheduleDate")

	rnd := make([]int64, 0)
	for range ids {
		rnd = append(rnd, Common.Random.Int63())
	}
	var request = tg.MessagesForwardMessagesRequest{
		FromPeer:     fromPeer,
		ToPeer:       toPeer,
		ID:           ids,
		Silent:       silent,
		Background:   background,
		WithMyScore:  withMyScope,
		ScheduleDate: scheduleDate,
		RandomID:     rnd,
	}
	updateClass, err := requestContext.Raw.MessagesForwardMessages(requestContext.Context, &request)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}

	return &Common.Result{
		Ok:         true,
		Data:       serialize.UpdatesClass(updateClass),
		NativeData: updateClass,
	}
}

func MessagesGetAllChats(requestContext *RequestContext) *Common.Result {
	id, errId := requestContext.Data.GetInt64("chatId")
	if errId != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errId,
		}
	}
	var ids = []int64{id}
	messagesChatsClass, err := requestContext.Raw.MessagesGetAllChats(requestContext.Context, ids)
	if err != nil {
		fmt.Println("updateClass, err", messagesChatsClass, err)
	}
	switch chats := messagesChatsClass.(type) {
	case *tg.MessagesChats:
		fmt.Println("chats", len(chats.Chats), chats.Chats[0])
		Db.SaveList(chats.Chats)
	case *tg.MessagesChatsSlice:
		fmt.Println("chats slice", len(chats.Chats), chats.Count)
		Db.SaveList(chats.Chats)
	}

	data := serialize.MessagesChatsClass(messagesChatsClass)
	return &Common.Result{
		Ok:         true,
		Data:       data,
		NativeData: messagesChatsClass,
	}
}

func MessagesGetChats(requestContext *RequestContext) *Common.Result {
	var ids []int64

	if id, errId := requestContext.Data.GetInt64("chatId"); errId == nil {
		ids = []int64{id}
	} else if idList, errIdList := requestContext.Data.GetInt64List("chatIdList"); errIdList == nil {
		ids = idList
	} else {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        Common.ParameterTypeInvalidError{Parameter: "chatId or chatIdList"},
		}
	}
	for index, id := range ids {
		ids[index] = Common.ConvertChatIdToUser(id)
	}
	messagesChatsClass, err := requestContext.Raw.MessagesGetChats(requestContext.Context, ids)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	switch chats := messagesChatsClass.(type) {
	case *tg.MessagesChats:
		Db.SaveList(chats.Chats)
	case *tg.MessagesChatsSlice:
		Db.SaveList(chats.Chats)
	}

	data := serialize.MessagesChatsClass(messagesChatsClass)
	return &Common.Result{
		Ok:         true,
		Data:       data,
		NativeData: messagesChatsClass,
	}
}

func MessagesGetCommonChats(requestContext *RequestContext) *Common.Result {
	user, err := deserialize.GetInputUserClass(requestContext.Data)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	request := tg.MessagesGetCommonChatsRequest{
		UserID: user,
	}
	if maxId, errMaxId := requestContext.Data.GetInt64("maxId"); errMaxId == nil && maxId > 0 {
		request.MaxID = maxId
	}
	if limit, errLimit := requestContext.Data.GetInt("limit"); errLimit == nil && limit > 0 {
		request.Limit = limit
	}

	messagesChatsClass, err := requestContext.Raw.MessagesGetCommonChats(requestContext.Context, &request)
	if err != nil {
		fmt.Println("updateClass, err", messagesChatsClass, err)
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	switch chats := messagesChatsClass.(type) {
	case *tg.MessagesChats:
		fmt.Println("chats", len(chats.Chats), chats.Chats)
		Db.SaveList(chats.Chats)
	case *tg.MessagesChatsSlice:
		fmt.Println("chats slice", len(chats.Chats), chats.Count)
		Db.SaveList(chats.Chats)
	}

	data := serialize.MessagesChatsClass(messagesChatsClass)
	return &Common.Result{
		Ok:         true,
		Data:       data,
		NativeData: messagesChatsClass,
	}
}

func MessagesGetFullChat(requestContext *RequestContext) *Common.Result {
	chatId, errId := requestContext.Data.GetChatId("chatId")
	if errId != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        errId,
		}
	}
	messagesChatFull, err := requestContext.Raw.MessagesGetFullChat(requestContext.Context, chatId)
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.ApiError,
			Error:        err,
		}
	}
	return &Common.Result{
		Ok:         true,
		Data:       serialize.MessagesChatFull(messagesChatFull),
		NativeData: messagesChatFull,
	}
}

func MessagesReportSpam(requestContext *RequestContext) *Common.Result {
	peer, err := deserialize.GetInputPeer(requestContext.Data, "peer")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	result, errApi := requestContext.Raw.MessagesReportSpam(requestContext.Context, peer)
	return &Common.Result{
		Ok:    result,
		Error: errApi,
	}
}

func MessagesSearchGifs(requestContext *RequestContext) *Common.Result {
	query, errQuery := requestContext.Data.GetString("query")
	if errQuery != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errQuery,
		}
	}
	offset, errOffset := requestContext.Data.GetInt("offset")
	if errOffset != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        errOffset,
		}
	}

	fmt.Println("MessagesSearchGifs", query, offset)
	return &Common.Result{
		Ok: false,
	}
}

func MessagesGetOnlines(requestContext *RequestContext) *Common.Result {
	peer, err := deserialize.GetInputPeer(requestContext.Data, "peer")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	onlines, errApi := requestContext.Raw.MessagesGetOnlines(requestContext.Context, peer)
	return &Common.Result{
		Ok:    true,
		Data:  serialize.ChatOnlines(onlines),
		Error: errApi,
	}
}

func MessagesGetMessagesReactions(requestContext *RequestContext) *Common.Result {
	peer, err := deserialize.GetInputPeer(requestContext.Data, "peer")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}

	messageIds, err := requestContext.Data.GetIntList("messageIds")

	request := &tg.MessagesGetMessagesReactionsRequest{
		Peer: peer,
		ID:   messageIds,
	}

	updatesClass, errApi := requestContext.Raw.MessagesGetMessagesReactions(requestContext.Context, request)
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(0, nil, nil)
	data := serialize.Updates(updatesClass, &serializeCtx)

	return &Common.Result{
		Ok:    true,
		Data:  data,
		Error: errApi,
	}
}

func MessagesGetMessageReactionsList(requestContext *RequestContext) *Common.Result {
	peer, err := deserialize.GetInputPeer(requestContext.Data, "peer")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	id, err := requestContext.Data.GetInt("id")
	if err != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.InvalidParameter,
			Error:        err,
		}
	}
	reaction, err := requestContext.Data.GetString("reaction")
	offset, err := requestContext.Data.GetString("offset")
	limit, err := requestContext.Data.GetInt("limit")

	request := &tg.MessagesGetMessageReactionsListRequest{
		Peer:     peer,
		ID:       id,
		Reaction: reaction,
		Offset:   offset,
		Limit:    limit,
	}

	reactionsList, errApi := requestContext.Raw.MessagesGetMessageReactionsList(requestContext.Context, request)
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(0, reactionsList.Users, nil)
	data := serialize.MessagesMessageReactionsList(reactionsList, &serializeCtx)

	return &Common.Result{
		Ok:    true,
		Data:  data,
		Error: errApi,
	}
}
