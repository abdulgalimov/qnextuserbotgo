package serialize

import "github.com/gotd/td/tg"

func _serializePaymentRequestedInfo(info *tg.PaymentRequestedInfo, parentData JsonData) {
	var data = makeJsonData(info)
	_serializeString(data, info.Name, "name")
	_serializeString(data, info.Phone, "phone")
	_serializeString(data, info.Email, "email")
	_serializePostAddress(&info.ShippingAddress, data)

	parentData["info"] = data
}

func _serializePostAddress(postAddress *tg.PostAddress, parentData JsonData) {
	var data = makeJsonData(postAddress)
	_serializeString(data, postAddress.StreetLine1, "streetLine1")
	_serializeString(data, postAddress.StreetLine2, "streetLine2")
	_serializeString(data, postAddress.City, "city")
	_serializeString(data, postAddress.State, "state")
	_serializeString(data, postAddress.CountryIso2, "country")
	_serializeString(data, postAddress.PostCode, "postCode")

	parentData["shippingAddress"] = data
}

func _serializePaymentCharge(paymentCharge *tg.PaymentCharge, parentData JsonData) {
	var data = makeJsonData(paymentCharge)
	_serializeString(data, paymentCharge.ID, "id")
	_serializeString(data, paymentCharge.ProviderChargeID, "providerChargeID")

	parentData["charge"] = data
}
