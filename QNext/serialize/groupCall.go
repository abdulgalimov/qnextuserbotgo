package serialize

import (
	"github.com/gotd/td/tg"
)

func _serializeGroupCall(ctx *Context, parentData JsonData, callClass tg.GroupCallClass) {
	if callClass == nil {
		return
	}
	var data = makeJsonData(callClass)
	switch call := callClass.(type) {
	case *tg.GroupCall:
		data["participantsCount"] = call.ParticipantsCount
		_serializeString(data, call.Title, "title")
		_serializeIdInt64(data, call.ID)
		_serializeInt64s(data, call.AccessHash, "accessHash")
		_serializeBool(data, call.JoinMuted, "joinMuted")
		_serializeBool(data, call.CanChangeJoinMuted, "сanChangeJoinMuted")
		_serializeBool(data, call.JoinDateAsc, "joinDateAsc")
		_serializeBool(data, call.ScheduleStartSubscribed, "scheduleStartSubscribed")
		_serializeBool(data, call.CanStartVideo, "canStartVideo")
		_serializeInt(data, call.ParticipantsCount, "participantsCount")
		_serializeInt(data, call.RecordStartDate, "recordStartDate")
		_serializeInt(data, call.ScheduleDate, "scheduleDate")
		_serializeInt(data, call.UnmutedVideoCount, "unmutedVideoCount")
		_serializeInt(data, call.UnmutedVideoLimit, "unmutedVideoLimit")
		_serializeInt(data, call.Version, "version")
	case *tg.GroupCallDiscarded:
		data["duration"] = call.Duration
		_serializeIdInt64(data, call.ID)
		_serializeInt64s(data, call.AccessHash, "accessHash")
	default:
		UndefinedClass(data, call)
	}
	parentData["call"] = data
}

func _serializeGroupCallParticipant(ctx *Context, participant *tg.GroupCallParticipant) JsonData {
	var data = makeJsonData(participant)
	_serializePeer(ctx, participant.Peer, data, "")
	_serializeBool(data, participant.Muted, "muted")
	_serializeBool(data, participant.Left, "left")
	_serializeBool(data, participant.CanSelfUnmute, "canSelfUnmute")
	_serializeBool(data, participant.JustJoined, "justJoined")
	_serializeBool(data, participant.MutedByYou, "mutedByYou")
	_serializeBool(data, participant.VolumeByAdmin, "volumeByAdmin")
	_serializeBool(data, participant.Self, "self")
	_serializeInt(data, participant.Date, "date")
	_serializeInt(data, participant.ActiveDate, "activeDate")
	_serializeInt(data, participant.Source, "source")
	_serializeInt(data, participant.Volume, "volume")
	_serializeString(data, participant.About, "about")
	_serializeInt64s(data, participant.RaiseHandRating, "raiseHandRating")
	return data
}

func _serializeGroupCallParticipantList(ctx *Context, participants []tg.GroupCallParticipant, parentData JsonData) {
	var count = len(participants)
	var data = make([]JsonData, count)
	for index, participant := range participants {
		data[index] = _serializeGroupCallParticipant(ctx, &participant)
	}

	parentData["participants"] = data
}

func _serializeInputGroupCall(inputGroupCall *tg.InputGroupCall, parentData JsonData) {
	var data = makeJsonData(inputGroupCall)
	_serializeIdInt64(data, inputGroupCall.ID)
	_serializeInt64s(data, inputGroupCall.AccessHash, "accessHash")

	parentData["call"] = data
}
