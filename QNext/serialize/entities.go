package serialize

import "github.com/gotd/td/tg"

func _serializeEntities(ctx *Context, entities []tg.MessageEntityClass, parentData JsonData) {
	if len(entities) == 0 {
		return
	}
	var data = make([]JsonData, len(entities))
	for index, entityClass := range entities {
		var entityData = makeJsonData(entityClass)
		data[index] = entityData
		switch entity := entityClass.(type) {
		case *tg.MessageEntityMention:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityHashtag:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityBotCommand:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityURL:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityEmail:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityBold:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityItalic:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityCode:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityPre:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
			entityData["language"] = entity.Language
		case *tg.MessageEntityTextURL:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
			entityData["url"] = entity.URL
		case *tg.MessageEntityMentionName:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
			entityData["userId"] = entity.UserID
		case *tg.InputMessageEntityMentionName:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
			entityData["user"] = _serializeInputUser(ctx, entity.UserID)
		case *tg.MessageEntityPhone:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityCashtag:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityUnderline:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityStrike:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityBlockquote:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityBankCard:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		case *tg.MessageEntityUnknown:
			entityData["offset"] = entity.Offset
			entityData["length"] = entity.Length
		default:
			UndefinedClass(entityData, entity)
		}
		parentData["entities"] = data
	}
}
