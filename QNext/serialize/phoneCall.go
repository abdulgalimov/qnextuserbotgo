package serialize

import "github.com/gotd/td/tg"

func _serializePhoneCall(ctx *Context, phoneCallClass tg.PhoneCallClass, parentData JsonData) {
	if phoneCallClass == nil {
		return
	}

	var data = makeJsonData(phoneCallClass)
	switch phoneCall := phoneCallClass.(type) {
	case *tg.PhoneCallEmpty:
		_serializeIdInt64(data, phoneCall.ID)
	case *tg.PhoneCallWaiting:
		_serializeIdInt64(data, phoneCall.ID)
		_serializeInt64s(data, phoneCall.AccessHash, "accessHash")
		_serializeBool(data, phoneCall.Video, "video")
		_serializeInt(data, phoneCall.Date, "date")
		_serializeInt(data, phoneCall.ReceiveDate, "receiveDate")
		_serializeUserId(ctx, phoneCall.AdminID, data, "admin")
		_serializeUserId(ctx, phoneCall.ParticipantID, data, "participant")
	case *tg.PhoneCallRequested:
		_serializeIdInt64(data, phoneCall.ID)
		_serializeInt64s(data, phoneCall.AccessHash, "accessHash")
		_serializeBool(data, phoneCall.Video, "video")
		_serializeInt(data, phoneCall.Date, "date")
		_serializeUserId(ctx, phoneCall.AdminID, data, "admin")
		_serializeUserId(ctx, phoneCall.ParticipantID, data, "participant")
	case *tg.PhoneCallAccepted:
		_serializeIdInt64(data, phoneCall.ID)
		_serializeInt64s(data, phoneCall.AccessHash, "accessHash")
		_serializeBool(data, phoneCall.Video, "video")
		_serializeInt(data, phoneCall.Date, "date")
		_serializeUserId(ctx, phoneCall.AdminID, data, "admin")
		_serializeUserId(ctx, phoneCall.ParticipantID, data, "participant")
	case *tg.PhoneCall:
		_serializeIdInt64(data, phoneCall.ID)
		_serializeInt64s(data, phoneCall.AccessHash, "accessHash")
		_serializeBool(data, phoneCall.Video, "video")
		_serializeInt(data, phoneCall.Date, "date")
		_serializeInt(data, phoneCall.StartDate, "startDate")
		_serializeUserId(ctx, phoneCall.AdminID, data, "admin")
		_serializeUserId(ctx, phoneCall.ParticipantID, data, "participant")
	case *tg.PhoneCallDiscarded:
		_serializeIdInt64(data, phoneCall.ID)
		_serializeBool(data, phoneCall.Video, "video")
		_serializeBool(data, phoneCall.NeedRating, "needRating")
		_serializeInt(data, phoneCall.Duration, "duration")
		_serializePhoneCallDiscardReasonClass(phoneCall.Reason, data)
	default:
		UndefinedClass(data, phoneCall)
	}
	parentData["phoneCall"] = data
}

func _serializePhoneCallDiscardReasonClass(reasonClass tg.PhoneCallDiscardReasonClass, parentData JsonData) {
	if reasonClass == nil {
		return
	}
	var data = makeJsonData(reasonClass)
	switch reason := reasonClass.(type) {
	case *tg.PhoneCallDiscardReasonMissed:
		break
	case *tg.PhoneCallDiscardReasonDisconnect:
		break
	case *tg.PhoneCallDiscardReasonHangup:
		break
	case *tg.PhoneCallDiscardReasonBusy:
		break
	default:
		UndefinedClass(data, reason)
	}

	parentData["reason"] = data
}
