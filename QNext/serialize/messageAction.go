package serialize

import (
	"github.com/gotd/td/tg"
)

func _serializeMessageAction(ctx *Context, actionClass tg.MessageActionClass, parentData JsonData) {
	var data = makeJsonData(actionClass)
	switch action := actionClass.(type) {
	case *tg.MessageActionChatCreate:
		data["title"] = action.Title
		data["users"] = action.Users
		break
	case *tg.MessageActionChatEditTitle:
		data["title"] = action.Title
		break
	case *tg.MessageActionChatEditPhoto:
		_serializePhoto(action.Photo, data)
		break
	case *tg.MessageActionChatDeletePhoto:
		break
	case *tg.MessageActionChatAddUser:
		data["users"] = action.Users
		break
	case *tg.MessageActionChatDeleteUser:
		data["userId"] = action.UserID
		break
	case *tg.MessageActionChatJoinedByLink:
		data["inviterId"] = action.InviterID
		break
	case *tg.MessageActionChannelCreate:
		data["title"] = action.Title
		break
	case *tg.MessageActionChatMigrateTo:
		data["channelId"] = action.ChannelID
		break
	case *tg.MessageActionChannelMigrateFrom:
		data["title"] = action.Title
		data["chatId"] = action.ChatID
		break
	case *tg.MessageActionPinMessage:
		break
	case *tg.MessageActionHistoryClear:
		break
	case *tg.MessageActionGameScore:
		data["gameId"] = action.GameID
		data["score"] = action.Score
		break
	case *tg.MessageActionPaymentSentMe:
		data["currency"] = action.Currency
		data["totalAmount"] = action.TotalAmount
		data["payload"] = string(action.Payload)
		data["shippingOptionID"] = action.ShippingOptionID
		_serializePaymentRequestedInfo(&action.Info, data)
		_serializePaymentCharge(&action.Charge, data)
		break
	case *tg.MessageActionPaymentSent:
		data["currency"] = action.Currency
		data["totalAmount"] = action.TotalAmount
		break
	case *tg.MessageActionPhoneCall:
		data["video"] = action.Video
		data["callId"] = action.CallID
		data["duration"] = action.Duration
		_serializePhoneCallDiscardReasonClass(action.Reason, data)
		break
	case *tg.MessageActionScreenshotTaken:
		break
	case *tg.MessageActionCustomAction:
		data["message"] = action.Message
		break
	case *tg.MessageActionBotAllowed:
		data["domain"] = action.Domain
		break
	case *tg.MessageActionSecureValuesSentMe:
		break
	case *tg.MessageActionSecureValuesSent:
		break
	case *tg.MessageActionContactSignUp:
		break
	case *tg.MessageActionGeoProximityReached:
		data["distance"] = action.Distance
		_serializePeer(ctx, action.FromID, data, "from")
		_serializePeer(ctx, action.ToID, data, "to")
		break
	case *tg.MessageActionGroupCall:
		data["duration"] = action.Duration
		_serializeInputGroupCall(&action.Call, data)
		break
	case *tg.MessageActionInviteToGroupCall:
		data["users"] = action.Users
		_serializeInputGroupCall(&action.Call, data)
		break
	case *tg.MessageActionSetMessagesTTL:
		data["period"] = action.Period
		break
	default:
		UndefinedClass(data, action)
	}

	parentData["action"] = data
}
