package serialize

import "github.com/gotd/td/tg"

func _serializePoll(poll *tg.Poll) JsonData {
	var data = makeJsonData(poll)
	_serializeInt64s(data, poll.ID, "id")
	_serializeBool(data, poll.Closed, "closed")
	_serializeBool(data, poll.PublicVoters, "publicVoters")
	_serializeBool(data, poll.MultipleChoice, "multipleChoice")
	_serializeBool(data, poll.Quiz, "quiz")
	_serializeString(data, poll.Question, "question")
	_serializeInt(data, poll.ClosePeriod, "closePeriod")
	_serializeInt(data, poll.CloseDate, "closeDate")
	var answers = make([]JsonData, len(poll.Answers))
	for index, answer := range poll.Answers {
		ansData := make(JsonData)
		ansData["text"] = answer.Text
		answers[index] = ansData
	}
	data["answers"] = answers
	return data
}

func _serializePollResults(ctx *Context, pollResults *tg.PollResults) JsonData {
	var data = makeJsonData(pollResults)
	_serializeBool(data, pollResults.Min, "min")
	_serializeInt(data, pollResults.TotalVoters, "totalVoters")
	_serializeString(data, pollResults.Solution, "solution")
	_serializeEntities(ctx, pollResults.SolutionEntities, data)

	data["recentVoters"] = pollResults.RecentVoters
	return data
}
