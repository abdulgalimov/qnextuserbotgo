package serialize

import (
	"github.com/gotd/td/tg"
)

func _serializeChatInviteExported(ctx *Context, chatInviteExportedClass *tg.ChatInviteExported, parentData JsonData) {
	var data = makeJsonData(chatInviteExportedClass)
	_serializeBool(data, chatInviteExportedClass.Revoked, "revoked")
	_serializeBool(data, chatInviteExportedClass.Permanent, "permanent")
	_serializeString(data, chatInviteExportedClass.Link, "link")
	_serializeUserId(ctx, chatInviteExportedClass.AdminID, data, "admin")
	_serializeInt(data, chatInviteExportedClass.Date, "date")
	_serializeInt(data, chatInviteExportedClass.StartDate, "startDate")
	_serializeInt(data, chatInviteExportedClass.ExpireDate, "expireDate")
	_serializeInt(data, chatInviteExportedClass.UsageLimit, "usageLimit")
	_serializeInt(data, chatInviteExportedClass.Usage, "usage")
	parentData["invite"] = data
}

func _serializeChatParticipantsClass(ctx *Context, parentData JsonData, participantsClass tg.ChatParticipantsClass) {
	if participantsClass == nil {
		return
	}
	var data = makeJsonData(participantsClass)
	switch participants := participantsClass.(type) {
	case *tg.ChatParticipantsForbidden:
		_serializeChatId(ctx, participants.ChatID, data, "")
		_serializeChatParticipantClass(ctx, participants.SelfParticipant, data, "selfParticipant")
	case *tg.ChatParticipants:
		_serializeChatId(ctx, participants.ChatID, data, "")
		data["participants"] = _serializeChatParticipantList(ctx, participants.Participants)
	default:
		UndefinedClass(data, participants)
	}
	parentData["chatParticipants"] = data
}

func _serializeChatParticipantClass(ctx *Context, participantClass tg.ChatParticipantClass, parentData JsonData, key string) JsonData {
	if participantClass == nil {
		return nil
	}
	var data = makeJsonData(participantClass)
	switch participant := participantClass.(type) {
	case *tg.ChatParticipant:
		_serializeUserId(ctx, participant.UserID, data, "")
		_serializeUserId(ctx, participant.InviterID, data, "inviter")
	case *tg.ChatParticipantCreator:
		_serializeUserId(ctx, participant.UserID, data, "")
	case *tg.ChatParticipantAdmin:
		_serializeUserId(ctx, participant.UserID, data, "")
		_serializeUserId(ctx, participant.InviterID, data, "inviter")
		_serializeInt(data, participant.Date, "date")
	default:
		UndefinedClass(data, participant)
	}
	if parentData != nil {
		parentData[key] = data
	}
	return data
}
func _serializeChannelParticipantClass(ctx *Context, participantClass tg.ChannelParticipantClass, parentData JsonData, key string) JsonData {
	if participantClass == nil {
		return nil
	}
	var data = makeJsonData(participantClass)
	switch participant := participantClass.(type) {
	case *tg.ChannelParticipant:
		_serializeUserId(ctx, participant.UserID, data, "user")
		_serializeInt(data, participant.Date, "date")
	case *tg.ChannelParticipantSelf:
		_serializeUserId(ctx, participant.UserID, data, "user")
		_serializeUserId(ctx, participant.InviterID, data, "inviter")
		_serializeInt(data, participant.Date, "date")
	case *tg.ChannelParticipantCreator:
		_serializeUserId(ctx, participant.UserID, data, "")
		_serializeString(data, participant.Rank, "rank")
		data["adminRights"] = _serializeChatAdminRights(&participant.AdminRights)
	case *tg.ChannelParticipantAdmin:
		_serializeUserId(ctx, participant.UserID, data, "user")
		_serializeUserId(ctx, participant.InviterID, data, "inviter")
		_serializeUserId(ctx, participant.PromotedBy, data, "promotedBy")
		_serializeInt(data, participant.Date, "date")
		_serializeBool(data, participant.CanEdit, "canEdit")
		_serializeBool(data, participant.Self, "self")
		_serializeString(data, participant.Rank, "rank")
		data["adminRights"] = _serializeChatAdminRights(&participant.AdminRights)
	case *tg.ChannelParticipantBanned:
		_serializeInt(data, participant.Date, "date")
		_serializeBool(data, participant.Left, "left")
		_serializePeer(ctx, participant.Peer, data, "peer")
		_serializeUserId(ctx, participant.KickedBy, data, "kickedBy")
		data["bannedRights"] = _serializeChatBannedRights(&participant.BannedRights)
	case *tg.ChannelParticipantLeft:
		_serializePeer(ctx, participant.Peer, data, "peer")
	default:
		UndefinedClass(data, participant)
	}
	if parentData != nil {
		parentData[key] = data
	}
	return data
}

func _serializeChatAdminRights(chatAdminRights *tg.ChatAdminRights) JsonData {
	var data = makeJsonData(chatAdminRights)
	_serializeBool(data, chatAdminRights.ChangeInfo, "changeInfo")
	_serializeBool(data, chatAdminRights.PostMessages, "postMessages")
	_serializeBool(data, chatAdminRights.EditMessages, "editMessages")
	_serializeBool(data, chatAdminRights.DeleteMessages, "deleteMessages")
	_serializeBool(data, chatAdminRights.BanUsers, "banUsers")
	_serializeBool(data, chatAdminRights.InviteUsers, "inviteUsers")
	_serializeBool(data, chatAdminRights.PinMessages, "pinMessages")
	_serializeBool(data, chatAdminRights.AddAdmins, "addAdmins")
	_serializeBool(data, chatAdminRights.Anonymous, "anonymous")
	_serializeBool(data, chatAdminRights.ManageCall, "manageCall")
	_serializeBool(data, chatAdminRights.Other, "other")
	return data
}

func _serializeChatBannedRights(chatBannedRights *tg.ChatBannedRights) JsonData {
	var data = makeJsonData(chatBannedRights)
	_serializeBool(data, chatBannedRights.ViewMessages, "viewMessages")
	_serializeBool(data, chatBannedRights.SendMessages, "sendMessages")
	_serializeBool(data, chatBannedRights.SendMedia, "sendMedia")
	_serializeBool(data, chatBannedRights.SendStickers, "sendStickers")
	_serializeBool(data, chatBannedRights.SendGifs, "sendGifs")
	_serializeBool(data, chatBannedRights.SendGames, "sendGames")
	_serializeBool(data, chatBannedRights.SendInline, "sendInline")
	_serializeBool(data, chatBannedRights.EmbedLinks, "embedLinks")
	_serializeBool(data, chatBannedRights.SendPolls, "sendPolls")
	_serializeBool(data, chatBannedRights.ChangeInfo, "changeInfo")
	_serializeBool(data, chatBannedRights.InviteUsers, "inviteUsers")
	_serializeBool(data, chatBannedRights.PinMessages, "pinMessages")
	_serializeInt(data, chatBannedRights.UntilDate, "untilDate")
	return data
}

func _serializeChatParticipantList(ctx *Context, participantClassList []tg.ChatParticipantClass) []JsonData {
	var list = make([]JsonData, len(participantClassList))

	for index, participant := range participantClassList {
		list[index] = _serializeChatParticipantClass(ctx, participant, nil, "")
	}
	return list
}

func _serializeChatFullClass(ctx *Context, chatFullClass tg.ChatFullClass, parentData JsonData, key string) {
	var data = makeJsonData(chatFullClass)
	switch chatFull := chatFullClass.(type) {
	case *tg.ChatFull:
		_serializeChatIdToBotId(data, chatFull.ID, "id")
		_serializeBotInfoList(data, chatFull.BotInfo)
		_serializeBool(data, chatFull.CanSetUsername, "canSetUsername")
		_serializeBool(data, chatFull.HasScheduled, "hasScheduled")
		_serializeString(data, chatFull.About, "about")
		_serializePhoto(chatFull.ChatPhoto, data)
		_serializeInt(data, chatFull.PinnedMsgID, "pinnedMsgID")
		_serializeInt(data, chatFull.FolderID, "folderID")
		_serializeInputGroupCall(&chatFull.Call, data)
		_serializeChatParticipantsClass(ctx, data, chatFull.Participants)
		_serializeChatInviteExported(ctx, &chatFull.ExportedInvite, data)
	case *tg.ChannelFull:
		_serializeChannelIdToBotId(data, chatFull.ID, "id")
		_serializeBotInfoList(data, chatFull.BotInfo)
		_serializeBool(data, chatFull.CanViewParticipants, "canViewParticipants")
		_serializeBool(data, chatFull.CanSetUsername, "canSetUsername")
		_serializeBool(data, chatFull.CanSetStickers, "canSetStickers")
		_serializeBool(data, chatFull.HiddenPrehistory, "hiddenPrehistory")
		_serializeBool(data, chatFull.CanSetLocation, "canSetLocation")
		_serializeBool(data, chatFull.HasScheduled, "hasScheduled")
		_serializeBool(data, chatFull.CanViewStats, "canViewStats")
		_serializeBool(data, chatFull.Blocked, "blocked")
		_serializeString(data, chatFull.About, "about")
		_serializeInt(data, chatFull.ParticipantsCount, "participantsCount")
		_serializeInt(data, chatFull.AdminsCount, "adminsCount")
		_serializeInt(data, chatFull.KickedCount, "kickedCount")
		_serializeInt(data, chatFull.BannedCount, "bannedCount")
		_serializeInt(data, chatFull.OnlineCount, "onlineCount")
		_serializeInt(data, chatFull.UnreadCount, "unreadCount")
		_serializeInt64s(data, chatFull.MigratedFromChatID, "migratedFromChatID")
		_serializeInt(data, chatFull.PinnedMsgID, "pinnedMessageId")
		_serializeInt(data, chatFull.SlowmodeSeconds, "slowmodeSeconds")
		_serializeInt(data, chatFull.SlowmodeNextSendDate, "slowmodeNextSendDate")
		_serializeInt(data, chatFull.FolderID, "folderID")
		_serializeChatId(ctx, chatFull.LinkedChatID, data, "linkedChat")
		_serializePhoto(chatFull.ChatPhoto, data)
		_serializeChatInviteExported(ctx, &chatFull.ExportedInvite, data)
	default:
		UndefinedClass(data, chatFull)
	}
	parentData[key] = data
}
