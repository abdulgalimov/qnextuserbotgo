package serialize

import (
	"encoding/base64"
	"fmt"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"strconv"
)

func _serializeBool(parentData JsonData, value bool, key string) bool {
	parentData[key] = value
	return true
}
func _serializeInt(parentData JsonData, value int, key string) bool {
	parentData[key] = value
	return true
}
func _serializeChatIdToBotId(parentData JsonData, value int64, key string) {
	parentData[key] = Common.ConvertChatIdToBot(value)
}
func _serializeChannelIdToBotId(parentData JsonData, value int64, key string) {
	parentData[key] = Common.ConvertChannelIdToBot(value)
}
func _serializeIdInt64(parentData JsonData, value int64) bool {
	if value != 0 {
		parentData["id"] = strconv.FormatInt(value, 10)
		return true
	}
	return false
}
func _serializeInt64s(parentData JsonData, value int64, key string) bool {
	if value != 0 {
		parentData[key] = strconv.FormatInt(value, 10)
		return true
	}
	return false
}
func _serializeInt64(parentData JsonData, value int64, key string) bool {
	parentData[key] = value
	return true
}
func _serializeFloat64s(parentData JsonData, value float64, key string) bool {
	if value != 0 {
		parentData[key] = fmt.Sprintf("%f", value)
		return true
	}
	return false
}
func _serializeString(parentData JsonData, value string, key string) bool {
	if value != "" {
		parentData[key] = value
		return true
	}
	return false
}
func _serializeStringBytes(parentData JsonData, value []byte, key string) bool {
	if len(value) > 0 {
		parentData[key] = base64.StdEncoding.EncodeToString(value)
		return true
	}
	return false
}

func _serializeSendMessageTypingAction(actionClass tg.SendMessageActionClass, parentData JsonData) {
	if actionClass == nil {
		return
	}
	var data = makeJsonData(actionClass)
	switch action := actionClass.(type) {
	case *tg.SendMessageTypingAction:
		break
	case *tg.SendMessageCancelAction:
		break
	case *tg.SendMessageRecordVideoAction:
		break
	case *tg.SendMessageUploadVideoAction:
		data["progress"] = action.Progress
		break
	case *tg.SendMessageRecordAudioAction:
		break
	case *tg.SendMessageUploadAudioAction:
		data["progress"] = action.Progress
		break
	case *tg.SendMessageRecordRoundAction:
		break
	case *tg.SendMessageUploadRoundAction:
		data["progress"] = action.Progress
		break
	case *tg.SendMessageUploadPhotoAction:
		data["progress"] = action.Progress
		break
	case *tg.SendMessageUploadDocumentAction:
		data["progress"] = action.Progress
		break
	case *tg.SendMessageGeoLocationAction:
		break
	case *tg.SendMessageChooseContactAction:
		break
	case *tg.SendMessageGamePlayAction:
		break
	default:
		UndefinedClass(data, action)
	}
	parentData["action"] = data
}
