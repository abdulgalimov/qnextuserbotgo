package serialize

import "github.com/gotd/td/tg"

func _serializeBotInfoList(parentData JsonData, botInfoList []tg.BotInfo) {
	var count = len(botInfoList)
	if count == 0 {
		return
	}
	var list = make([]JsonData, count)
	for index, botInfo := range botInfoList {
		list[index] = _serializeBotInfo(&botInfo, nil)
	}

	parentData["botInfo"] = list
}

func _serializeBotInfo(botInfo *tg.BotInfo, parentData JsonData) JsonData {
	if botInfo.UserID == 0 {
		return nil
	}
	var data = makeJsonData(botInfo)
	_serializeInt64(data, botInfo.UserID, "id")
	_serializeString(data, botInfo.Description, "description")
	_serializeBotCommands(botInfo.Commands, data, "commands")

	if parentData != nil {
		parentData["botInfo"] = data
	}
	return data
}

func _serializeBotCommands(botCommands []tg.BotCommand, parentData JsonData, key string) {
	var data = make([]JsonData, len(botCommands))
	for index, botCommand := range botCommands {
		var commandData = makeJsonData(&botCommand)
		data[index] = commandData
		_serializeString(commandData, botCommand.Command, "command")
		_serializeString(commandData, botCommand.Description, "description")
	}

	parentData[key] = data
}
