package serialize

import "github.com/gotd/td/tg"

type Constructor interface {
	TypeName() string
}

type JsonData map[string]interface{}

func makeJsonData(constructor Constructor) JsonData {
	var data = make(JsonData)
	data["constructor"] = constructor.TypeName()
	return data
}

type Context struct {
	Date  int
	Users []tg.UserClass
	Chats []tg.ChatClass
	Data  JsonData
}

func (c *Context) Init(date int, users []tg.UserClass, chats []tg.ChatClass) {
	c.Date = date
	c.Users = users
	c.Chats = chats
}
func (c *Context) Clear() {
	c.Users = nil
	c.Chats = nil
}
func (c *Context) FindUser(userId int64) *tg.User {
	if c.Users == nil {
		return nil
	}
	for _, userClass := range c.Users {
		switch user := userClass.(type) {
		case *tg.User:
			if user.ID == userId {
				return user
			}
		}
	}
	return nil
}
func (c *Context) FindChat(chatId int64) tg.ChatClass {
	if c.Chats == nil {
		return nil
	}
	for _, chatClass := range c.Chats {
		switch chat := chatClass.(type) {
		case *tg.Chat:
			if chat.ID == chatId {
				return chat
			}
		case *tg.ChatForbidden:
			if chat.ID == chatId {
				return chat
			}
		case *tg.Channel:
			if chat.ID == chatId {
				return chat
			}
		case *tg.ChannelForbidden:
			if chat.ID == chatId {
				return chat
			}
		}
	}
	return nil
}
