package serialize

import (
	"github.com/gotd/td/tg"
)

func _serializeMessage(ctx *Context, messageClass tg.MessageClass, parentData JsonData) {
	if messageClass == nil {
		return
	}
	var data = makeJsonData(messageClass)
	switch message := messageClass.(type) {
	case *tg.Message:
		_serializeInt(data, message.ID, "id")
		_serializeString(data, message.PostAuthor, "postAuthor")
		_serializeString(data, message.Message, "message")
		_serializeBool(data, message.Out, "out")
		_serializeBool(data, message.Mentioned, "mentioned")
		_serializeBool(data, message.Silent, "silent")
		_serializeBool(data, message.Post, "post")
		_serializeBool(data, message.Pinned, "pinned")
		_serializeBool(data, message.FromScheduled, "fromScheduled")
		_serializeInt(data, message.Date, "date")
		_serializeInt(data, message.EditDate, "editDate")
		_serializeInt(data, message.Views, "views")
		_serializeInt64(data, message.ViaBotID, "viaBotId")
		_serializeInt(data, message.Forwards, "forwards")
		_serializeInt64s(data, message.GroupedID, "groupedId")
		_serializeFwdHeader(ctx, &message.FwdFrom, data, "fwdFrom")
		_serializePeer(ctx, message.FromID, data, "")
		_serializePeer(ctx, message.PeerID, data, "")
		_serializeReplyTo(ctx, &message.ReplyTo, data)
		_serializeEntities(ctx, message.Entities, data)
		_serializeMedia(ctx, message.Media, data)
	case *tg.MessageService:
		_serializeInt(data, message.ID, "id")
		_serializeInt(data, message.Date, "date")
		_serializeBool(data, message.Out, "out")
		_serializeBool(data, message.Post, "post")
		_serializeBool(data, message.Mentioned, "mentioned")
		_serializeBool(data, message.Silent, "silent")
		_serializePeer(ctx, message.FromID, data, "from")
		_serializePeer(ctx, message.PeerID, data, "chat")
		_serializeReplyTo(ctx, &message.ReplyTo, data)
		_serializeMessageAction(ctx, message.Action, data)
	default:
		UndefinedClass(data, message)
	}
	parentData["message"] = data
}

func _serializeFwdHeader(ctx *Context, fwdClass *tg.MessageFwdHeader, parentData JsonData, key string) {
	if fwdClass.Date == 0 {
		return
	}
	var data = makeJsonData(fwdClass)
	_serializePeer(ctx, fwdClass.FromID, data, "")
	_serializeString(data, fwdClass.FromName, "fromName")
	_serializeString(data, fwdClass.PostAuthor, "postAuthor")
	_serializeInt(data, fwdClass.ChannelPost, "channelPost")
	_serializeInt(data, fwdClass.Date, "date")
	_serializeInt(data, fwdClass.SavedFromMsgID, "savedFromMsgID")

	parentData[key] = data
}

func _serializeReplyHeader(ctx *Context, replyClass *tg.MessageReplyHeader, parentData JsonData, key string) {
	var data = makeJsonData(replyClass)
	_serializeInt(data, replyClass.ReplyToMsgID, "replyToMsgID")
	_serializeInt(data, replyClass.ReplyToTopID, "replyToTopID")
	_serializePeer(ctx, replyClass.ReplyToPeerID, data, "replyToPeerID")

	parentData[key] = data
}

func _serializeReplyTo(ctx *Context, replyTo *tg.MessageReplyHeader, parentData JsonData) {
	if replyTo.ReplyToMsgID == 0 {
		return
	}
	var data = makeJsonData(replyTo)
	if replyTo.ReplyToMsgID == 0 {
		return
	}
	_serializeInt(data, replyTo.ReplyToMsgID, "messageId")
	_serializePeer(ctx, replyTo.ReplyToPeerID, parentData, "")

	parentData["reply"] = data
}
