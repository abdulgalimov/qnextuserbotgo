package serialize

import "github.com/gotd/td/tg"

func UserFull(userFull *tg.UserFull) JsonData {
	var data = makeJsonData(userFull)
	_serializeBool(data, userFull.Blocked, "blocked")
	_serializeBool(data, userFull.PhoneCallsAvailable, "phoneCallsAvailable")
	_serializeBool(data, userFull.PhoneCallsPrivate, "phoneCallsPrivate")
	_serializeBool(data, userFull.CanPinMessage, "canPinMessage")
	_serializeBool(data, userFull.HasScheduled, "gasScheduled")
	_serializeBool(data, userFull.VideoCallsAvailable, "videoCallsAvailable")
	_serializeInt(data, userFull.PinnedMsgID, "pinnedMsgID")
	_serializeInt(data, userFull.CommonChatsCount, "commonChatsCount")
	_serializeString(data, userFull.About, "about")
	_serializeBotInfo(&userFull.BotInfo, data)
	_serializeInt64(data, userFull.ID, "id")
	_serializePeerSettings(data, &userFull.Settings)

	return data
}

func _serializeUserClassList(userClassList []tg.UserClass) []JsonData {
	var list = make([]JsonData, len(userClassList))
	for index, userClass := range userClassList {
		list[index] = _serializeUserClass(userClass, nil, "")
	}
	return list
}

func _serializeUserClass(userClass tg.UserClass, parentData JsonData, key string) JsonData {
	var data JsonData
	switch user := userClass.(type) {
	case *tg.User:
		data = _serializeUser(user)
	}
	if parentData != nil {
		parentData[key] = data
	}
	return data
}

func _serializeUser(user *tg.User) JsonData {
	var data = makeJsonData(user)
	_serializeInt64(data, user.ID, "id")
	_serializeInt64s(data, user.AccessHash, "accessHash")
	_serializeString(data, user.LastName, "lastName")
	_serializeString(data, user.FirstName, "firstName")
	_serializeProfilePhoto(user.Photo, data)
	_serializeBool(data, user.Contact, "contact")
	_serializeBool(data, user.Deleted, "deleted")
	_serializeBool(data, user.Verified, "verified")
	_serializeBool(data, user.Scam, "scam")
	_serializeBool(data, user.Support, "support")
	_serializeBool(data, user.Fake, "fake")
	_serializeString(data, user.Username, "username")
	_serializeString(data, user.Phone, "phone")
	if user.Bot {
		var botData = make(JsonData)
		data["bot"] = botData
		_serializeBool(botData, user.BotChatHistory, "chatHistory")
		_serializeBool(botData, user.BotNochats, "noChats")
		_serializeBool(botData, user.BotInlineGeo, "inlineGeo")
		_serializeString(botData, user.BotInlinePlaceholder, "inlinePlaceholder")
		_serializeInt(botData, user.BotInfoVersion, "infoVersion")
	}
	if user.Restricted {
		data["restriction"] = _serializeRestrictionReasons(user.RestrictionReason)
	}
	if user.Status != nil {
		data["status"] = _serializeUserStatus(user.Status)
	}
	return data
}

func _serializeInputUser(ctx *Context, inputUserClass tg.InputUserClass) JsonData {
	var data = makeJsonData(inputUserClass)
	switch inputUser := inputUserClass.(type) {
	case *tg.InputUserEmpty:
		break
	case *tg.InputUserSelf:
		break
	case *tg.InputUser:
		_serializeInt64(data, inputUser.UserID, "id")
		_serializeInt64s(data, inputUser.AccessHash, "accessHash")
		break
	case *tg.InputUserFromMessage:
		_serializeInt64(data, inputUser.UserID, "id")
		_serializeInt(data, inputUser.MsgID, "messageId")
		_serializeInputPeer(ctx, inputUser.Peer, data, "peer")
		break
	}
	return data
}
