package serialize

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func _serializePeer(ctx *Context, peerClass tg.PeerClass, parentData JsonData, key string) {
	if peerClass == nil {
		return
	}
	var id interface{}
	var systemKey string
	var foundData JsonData
	switch peer := peerClass.(type) {
	case *tg.PeerUser:
		id = peer.UserID
		systemKey = "user"
		if ctx != nil {
			var user = ctx.FindUser(peer.UserID)
			if user != nil {
				foundData = _serializeUser(user)
			}
		}
		break
	case *tg.PeerChat:
		id = peer.ChatID
		systemKey = "chat"
		if ctx != nil {
			var chat = ctx.FindChat(peer.ChatID)
			if chat != nil {
				foundData = _serializeChatClass(chat)
			}
		}
		break
	case *tg.PeerChannel:
		id = Common.ConvertChannelIdToBot(peer.ChannelID)
		systemKey = "channel"
		if ctx != nil {
			var chat = ctx.FindChat(peer.ChannelID)
			if chat != nil {
				foundData = _serializeChatClass(chat)
			}
		}
		break
	}
	if key == "" {
		key = systemKey
	}
	if foundData != nil {
		parentData[key] = foundData
	} else {
		var data = makeJsonData(peerClass)
		data["id"] = id
		parentData[key] = data
	}
}

func _serializeChatClassList(chatClassList []tg.ChatClass) []JsonData {
	var list = make([]JsonData, len(chatClassList))
	for index, chatClass := range chatClassList {
		list[index] = _serializeChatClass(chatClass)
	}
	return list
}

func _serializeChatClass(chatClass tg.ChatClass) JsonData {
	var data = makeJsonData(chatClass)
	switch chat := chatClass.(type) {
	case *tg.ChatEmpty:
		_serializeInt64s(data, chat.ID, "id")
	case *tg.Chat:
		_serializeChatIdToBotId(data, chat.ID, "id")
		_serializeString(data, chat.Title, "title")
		_serializeBool(data, chat.Creator, "creator")
		_serializeBool(data, chat.Kicked, "kicked")
		_serializeBool(data, chat.Left, "left")
		_serializeBool(data, chat.Deactivated, "deactivated")
		_serializeBool(data, chat.CallActive, "callActive")
		_serializeBool(data, chat.CallNotEmpty, "callNotEmpty")
		_serializeInt(data, chat.ParticipantsCount, "participantsCount")
		_serializeInt(data, chat.Date, "date")
		_serializeInt(data, chat.Version, "version")
		data["adminRights"] = _serializeChatAdminRights(&chat.AdminRights)
		data["defaultBannedRights"] = _serializeChatBannedRights(&chat.DefaultBannedRights)
	case *tg.ChatForbidden:
		_serializeInt64s(data, chat.ID, "id")
		_serializeString(data, chat.Title, "title")
	case *tg.Channel:
		_serializeChannelIdToBotId(data, chat.ID, "id")
		_serializeInt64s(data, chat.AccessHash, "accessHash")
		_serializeString(data, chat.Title, "title")
		_serializeString(data, chat.Username, "username")
		_serializeBool(data, chat.Creator, "creator")
		_serializeBool(data, chat.Left, "left")
		_serializeBool(data, chat.Broadcast, "broadcast")
		_serializeBool(data, chat.Verified, "verified")
		_serializeBool(data, chat.Megagroup, "megagroup")
		_serializeBool(data, chat.Restricted, "restricted")
		_serializeBool(data, chat.Signatures, "signatures")
		_serializeBool(data, chat.Min, "min")
		_serializeBool(data, chat.Scam, "scam")
		_serializeBool(data, chat.HasLink, "hasLink")
		_serializeBool(data, chat.HasGeo, "hasGeo")
		_serializeBool(data, chat.SlowmodeEnabled, "slowmodeEnabled")
		_serializeBool(data, chat.CallActive, "callActive")
		_serializeBool(data, chat.CallNotEmpty, "callNotEmpty")
		_serializeBool(data, chat.Gigagroup, "gigagroup")
		_serializeInt64s(data, chat.AccessHash, "accessHash")
		_serializeInt(data, chat.ParticipantsCount, "participantsCount")
		_serializeInt(data, chat.Date, "date")
		data["restrictionReason"] = _serializeRestrictionReasons(chat.RestrictionReason)
		data["adminRights"] = _serializeChatAdminRights(&chat.AdminRights)
		data["bannedRights"] = _serializeChatBannedRights(&chat.BannedRights)
		data["defaultBannedRights"] = _serializeChatBannedRights(&chat.DefaultBannedRights)
	case *tg.ChannelForbidden:
		_serializeChannelIdToBotId(data, chat.ID, "id")
		_serializeString(data, chat.Title, "title")
		_serializeBool(data, chat.Broadcast, "broadcast")
		_serializeBool(data, chat.Megagroup, "megagroup")
		_serializeInt64s(data, chat.AccessHash, "accessHash")
		_serializeInt(data, chat.UntilDate, "untilDate")
	default:
		UndefinedClass(data, chat)
	}
	return data
}
func _serializeUserStatus(statusClass tg.UserStatusClass) JsonData {
	var data = makeJsonData(statusClass)
	switch status := statusClass.(type) {
	case *tg.UserStatusEmpty:
		break
	case *tg.UserStatusOffline:
		_serializeInt(data, status.WasOnline, "wasOnline")
		break
	case *tg.UserStatusOnline:
		_serializeInt(data, status.Expires, "expires")
		break
	case *tg.UserStatusRecently:
		break
	case *tg.UserStatusLastWeek:
		break
	case *tg.UserStatusLastMonth:
		break
	default:
		UndefinedClass(data, status)
	}
	return data
}

func _serializeRestrictionReasons(reasons []tg.RestrictionReason) JsonData {
	var data = make(JsonData)
	var reasonListData = make([]JsonData, len(reasons))
	data["reason"] = reasonListData
	for index, reason := range reasons {
		var reasonData = makeJsonData(&reason)
		reasonListData[index] = reasonData
		reasonData["reason"] = reason.Reason
		reasonData["text"] = reason.Text
		reasonData["platform"] = reason.Platform
	}
	return data
}

func _serializeUserId(ctx *Context, userId int64, parentData JsonData, key string) {
	if key == "" {
		key = "user"
	}
	if ctx != nil {
		var user = ctx.FindUser(userId)
		if user != nil {
			parentData[key] = _serializeUser(user)
			return
		}
	}
	var data = make(JsonData)
	_serializeInt64(data, userId, "id")
	parentData[key] = data
}
func _serializeChatId(ctx *Context, chatId int64, parentData JsonData, key string) {
	if key == "" {
		key = "chat"
	}
	if ctx != nil {
		var chat = ctx.FindChat(chatId)
		if chat != nil {
			parentData[key] = _serializeChatClass(chat)
			return
		}
	}
	var data = make(JsonData)
	_serializeChatIdToBotId(data, chatId, "id")
	parentData[key] = data
}

func _serializeChannelId(ctx *Context, chatId int64, parentData JsonData, key string) {
	if key == "" {
		key = "channel"
	}
	if ctx != nil {
		var chat = ctx.FindChat(chatId)
		if chat != nil {
			parentData[key] = _serializeChatClass(chat)
			return
		}
	}
	var data = make(JsonData)
	_serializeChatIdToBotId(data, chatId, "id")
	parentData[key] = data
}

func _serializePeerSettings(parentData JsonData, peerSettings *tg.PeerSettings) {
	var data = makeJsonData(peerSettings)
	var addSettings = false
	addSettings = _serializeBool(data, peerSettings.ReportSpam, "reportSpam") || addSettings
	addSettings = _serializeBool(data, peerSettings.AddContact, "addContact") || addSettings
	addSettings = _serializeBool(data, peerSettings.BlockContact, "blockContact") || addSettings
	addSettings = _serializeBool(data, peerSettings.ShareContact, "shareContact") || addSettings
	addSettings = _serializeBool(data, peerSettings.ReportGeo, "reportGeo") || addSettings
	addSettings = _serializeBool(data, peerSettings.InviteMembers, "inviteMembers") || addSettings
	addSettings = _serializeInt(data, peerSettings.GeoDistance, "geoDistance") || addSettings

	if addSettings {
		parentData["settings"] = data
	}
}

func _serializeInputPeer(ctx *Context, inputPeerClass tg.InputPeerClass, parentData JsonData, key string) {
	var data = makeJsonData(inputPeerClass)
	switch inputPeer := inputPeerClass.(type) {
	case *tg.InputPeerEmpty:
		break
	case *tg.InputPeerSelf:
		break
	case *tg.InputPeerChat:
		_serializeChatId(ctx, inputPeer.ChatID, data, "")
		break
	case *tg.InputPeerUser:
		_serializeUserId(ctx, inputPeer.UserID, data, "")
		break
	case *tg.InputPeerChannel:
		_serializeChannelId(ctx, inputPeer.ChannelID, data, "")
		break
	case *tg.InputPeerUserFromMessage:
		_serializeInt(data, inputPeer.MsgID, "messageId")
		_serializeUserId(ctx, inputPeer.UserID, data, "")
		break
	case *tg.InputPeerChannelFromMessage:
		_serializeInt(data, inputPeer.MsgID, "messageId")
		_serializeChannelId(ctx, inputPeer.ChannelID, data, "")
		break
	default:
		UndefinedClass(data, inputPeer)
	}

	parentData[key] = data
}
