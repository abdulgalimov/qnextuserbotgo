package serialize

import "github.com/gotd/td/tg"

func MessagesStickerSetClass(stickerSetClass tg.MessagesStickerSetClass) JsonData {
	switch value := stickerSetClass.(type) {
	case *tg.MessagesStickerSet:
		return MessagesStickerSet(value)
	}
	return makeJsonData(stickerSetClass)
}

func MessagesStickerSet(stickerSet *tg.MessagesStickerSet) JsonData {
	var data = makeJsonData(stickerSet)
	data["set"] = StickerSet(&stickerSet.Set)
	return data
}

func StickerSet(stickerSet *tg.StickerSet) JsonData {
	var data = makeJsonData(stickerSet)
	_serializeBool(data, stickerSet.Archived, "archived")
	_serializeBool(data, stickerSet.Official, "official")
	_serializeBool(data, stickerSet.Masks, "masks")
	_serializeBool(data, stickerSet.Animated, "animated")
	_serializeInt(data, stickerSet.InstalledDate, "installedDate")
	_serializeInt(data, stickerSet.ThumbVersion, "thumbVersion")
	_serializeInt64s(data, stickerSet.ID, "id")
	_serializeInt64s(data, stickerSet.AccessHash, "accessHash")
	_serializeString(data, stickerSet.Title, "title")
	_serializeString(data, stickerSet.ShortName, "shortName")
	_serializeInt(data, stickerSet.Count, "count")
	return data
}
