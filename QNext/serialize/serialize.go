package serialize

import (
	"fmt"
	"github.com/gotd/td/tg"
)

type UndefinedClassType interface {
	TypeName() string
}

func UndefinedClass(data JsonData, class UndefinedClassType) {
	//data["_warning"] = "Attention, Don't use this event. It will be removed in the future!"
	data["_info"] = class
}

func UserStatus(ctx *Context, update *tg.UpdateUserStatus) JsonData {
	var data = makeJsonData(update)
	data["event"] = "userStatus"
	_serializeUserId(ctx, update.UserID, data, "")
	data["userStatus"] = _serializeUserStatus(update.Status)
	return data
}
func UserName(ctx *Context, update *tg.UpdateUserName) JsonData {
	var data = makeJsonData(update)
	data["event"] = "userName"
	_serializeUserId(ctx, update.UserID, data, "")
	_serializeString(data, update.FirstName, "firstName")
	_serializeString(data, update.LastName, "lastName")
	_serializeString(data, update.Username, "username")
	return data
}
func UserPhone(ctx *Context, update *tg.UpdateUserPhone) JsonData {
	var data = makeJsonData(update)
	data["event"] = "userPhone"
	_serializeUserId(ctx, update.UserID, data, "")
	_serializeString(data, update.Phone, "phone")
	return data
}

func NewMessage(ctx *Context, update *tg.UpdateNewMessage) JsonData {
	var data = makeJsonData(update)
	data["event"] = "newMessage"
	_serializeMessage(ctx, update.Message, data)
	return data
}
func NewChannelMessage(ctx *Context, update *tg.UpdateNewChannelMessage) JsonData {
	var data = makeJsonData(update)
	data["event"] = "newChannelMessage"
	_serializeMessage(ctx, update.Message, data)
	return data
}
func EditChannelMessage(ctx *Context, update *tg.UpdateEditChannelMessage) JsonData {
	var data = makeJsonData(update)
	data["event"] = "editChannelMessage"
	_serializeMessage(ctx, update.Message, data)
	return data
}

func DeleteMessages(ctx *Context, update *tg.UpdateDeleteMessages) JsonData {
	var data = makeJsonData(update)
	data["event"] = "deleteMessages"
	data["messages"] = update.Messages
	return data
}
func DeleteChannelMessages(ctx *Context, update *tg.UpdateDeleteChannelMessages) JsonData {
	var data = makeJsonData(update)
	data["event"] = "deleteChannelMessages"
	data["messages"] = update.Messages
	_serializeChannelId(ctx, update.ChannelID, data, "channel")
	return data
}

func UserTyping(ctx *Context, update *tg.UpdateUserTyping) JsonData {
	var data = makeJsonData(update)
	data["event"] = "userTyping"
	_serializeUserId(ctx, update.UserID, data, "")
	_serializeSendMessageTypingAction(update.Action, data)
	return data
}

func ChatUserTyping(ctx *Context, update *tg.UpdateChatUserTyping) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatUserTyping"
	_serializeChatId(ctx, update.ChatID, data, "")
	_serializePeer(ctx, update.FromID, data, "from")
	_serializeSendMessageTypingAction(update.Action, data)
	return data
}
func UpdateChatParticipants(ctx *Context, update *tg.UpdateChatParticipants) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatParticipants"
	_serializeChatParticipantsClass(ctx, data, update.Participants)
	return data
}
func UpdateChatParticipant(ctx *Context, update *tg.UpdateChatParticipant) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatParticipant"
	_serializeInt(data, update.Date, "date")
	_serializeChatId(ctx, update.ChatID, data, "chat")
	_serializeUserId(ctx, update.ActorID, data, "actor")
	_serializeUserId(ctx, update.UserID, data, "user")
	_serializeChatParticipantClass(ctx, update.PrevParticipant, data, "prevParticipant")
	_serializeChatParticipantClass(ctx, update.NewParticipant, data, "newParticipant")
	_serializeChatInviteExported(ctx, &update.Invite, data)
	return data
}
func UpdateChatParticipantAdd(ctx *Context, update *tg.UpdateChatParticipantAdd) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatParticipantAdd"
	_serializeChatId(ctx, update.ChatID, data, "chat")
	_serializeUserId(ctx, update.UserID, data, "user")
	_serializeUserId(ctx, update.InviterID, data, "inviter")
	_serializeInt(data, update.Date, "date")
	return data
}
func UpdateChatParticipantDelete(ctx *Context, update *tg.UpdateChatParticipantDelete) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatParticipantDelete"
	_serializeChatId(ctx, update.ChatID, data, "chat")
	_serializeUserId(ctx, update.UserID, data, "user")
	return data
}
func UpdateChatParticipantAdmin(ctx *Context, update *tg.UpdateChatParticipantAdmin) JsonData {
	var data = makeJsonData(update)
	data["event"] = "chatParticipantAdmin"
	_serializeChatId(ctx, update.ChatID, data, "chat")
	_serializeUserId(ctx, update.UserID, data, "user")
	_serializeBool(data, update.IsAdmin, "isAdmin")
	return data
}

func ChannelMessageViews(ctx *Context, update *tg.UpdateChannelMessageViews) JsonData {
	var data = makeJsonData(update)
	data["event"] = "channelMessageViews"
	_serializeChannelId(ctx, update.ChannelID, data, "channel")
	data["messageId"] = update.ID
	data["views"] = update.Views
	return data
}
func ChannelParticipant(ctx *Context, update *tg.UpdateChannelParticipant) JsonData {
	var data = makeJsonData(update)
	data["event"] = "channelParticipant"
	_serializeInt(data, update.Date, "date")
	_serializeChannelId(ctx, update.ChannelID, data, "chat")
	_serializeUserId(ctx, update.ActorID, data, "actor")
	_serializeUserId(ctx, update.UserID, data, "user")
	_serializeChannelParticipantClass(ctx, update.PrevParticipant, data, "prevParticipant")
	_serializeChannelParticipantClass(ctx, update.NewParticipant, data, "newParticipant")
	_serializeChatInviteExported(ctx, &update.Invite, data)
	return data
}

func GroupCall(ctx *Context, update *tg.UpdateGroupCall) JsonData {
	var data = makeJsonData(update)
	data["event"] = "groupCall"
	_serializeChatId(ctx, update.ChatID, data, "chat")
	_serializeGroupCall(ctx, data, update.Call)
	return data
}

func GroupCallParticipants(ctx *Context, update *tg.UpdateGroupCallParticipants) JsonData {
	var data = makeJsonData(update)
	data["event"] = "groupCallParticipants"
	_serializeInputGroupCall(&update.Call, data)
	_serializeGroupCallParticipantList(ctx, update.Participants, data)
	return data
}

func UpdatePhoneCall(ctx *Context, update *tg.UpdatePhoneCall) JsonData {
	var data = makeJsonData(update)
	data["event"] = "phoneCall"
	_serializePhoneCall(ctx, update.PhoneCall, data)
	return data
}

func UpdateChannel(ctx *Context, update *tg.UpdateChannel) JsonData {
	var data = makeJsonData(update)
	data["event"] = "updateChannel"
	_serializeChannelId(ctx, update.ChannelID, data, "channel")
	return data
}

func UpdateChat(ctx *Context, update *tg.UpdateChat) JsonData {
	var data = makeJsonData(update)
	data["event"] = "updateChannel"
	_serializeChatId(ctx, update.ChatID, data, "chat")
	return data
}

func MessagesChatFull(messagesChatFull *tg.MessagesChatFull) JsonData {
	var data = makeJsonData(messagesChatFull)
	var ctx = Context{}
	ctx.Init(0, messagesChatFull.Users, messagesChatFull.Chats)
	_serializeChatFullClass(&ctx, messagesChatFull.FullChat, data, "chat")
	return data
}

func UpdatesClass(updatesClass tg.UpdatesClass) JsonData {
	var data = makeJsonData(updatesClass)
	var ctx = Context{}
	switch updates := updatesClass.(type) {
	case *tg.Updates:
		var count = len(updates.Updates)
		var list = make([]JsonData, count)
		data["updates"] = list
		if count > 0 {
			ctx.Init(0, updates.Users, updates.Chats)
			for index, update := range updates.Updates {
				list[index] = Update(update, &ctx)
			}
		}
		//
		data["chats"] = _serializeChatClassList(updates.Chats)
		data["users"] = _serializeUserClassList(updates.Users)
	case *tg.UpdateShortSentMessage:
		_serializeInt(data, updates.ID, "id")
		_serializeInt(data, updates.Date, "date")
		_serializeBool(data, updates.Out, "out")
		_serializeEntities(&ctx, updates.Entities, data)
		_serializeMedia(&ctx, updates.Media, data)
	}
	return data
}

func MessagesChatsClass(messagesChatsClass tg.MessagesChatsClass) JsonData {
	var data = makeJsonData(messagesChatsClass)
	switch chats := messagesChatsClass.(type) {
	case *tg.MessagesChats:
		data["chats"] = _serializeChatClassList(chats.Chats)
	case *tg.MessagesChatsSlice:
		_serializeInt(data, chats.Count, "count")
		data["chats"] = _serializeChatClassList(chats.Chats)
	}
	return data
}

func UpdateServiceNotification(ctx *Context, serviceNotification *tg.UpdateServiceNotification) JsonData {
	var data = makeJsonData(serviceNotification)
	_serializeBool(data, serviceNotification.Popup, "popup")
	_serializeInt(data, serviceNotification.InboxDate, "inboxDate")
	_serializeString(data, serviceNotification.Message, "message")
	_serializeString(data, serviceNotification.Type, "type")
	_serializeEntities(ctx, serviceNotification.Entities, data)
	_serializeMedia(ctx, serviceNotification.Media, data)
	return data
}

func UpdateShortMessage(ctx *Context, updateShortMessage *tg.UpdateShortMessage) JsonData {
	var data = makeJsonData(updateShortMessage)
	_serializeBool(data, updateShortMessage.Out, "out")
	_serializeBool(data, updateShortMessage.Mentioned, "mentioned")
	_serializeBool(data, updateShortMessage.MediaUnread, "mediaUnread")
	_serializeBool(data, updateShortMessage.Silent, "silent")
	_serializeInt(data, updateShortMessage.ID, "id")
	_serializeInt64(data, updateShortMessage.UserID, "userId")
	_serializeInt64(data, updateShortMessage.ViaBotID, "viaBotId")
	_serializeString(data, updateShortMessage.Message, "message")
	_serializeInt(data, updateShortMessage.Date, "date")
	_serializeFwdHeader(ctx, &updateShortMessage.FwdFrom, data, "fwdFrom")
	_serializeReplyTo(ctx, &updateShortMessage.ReplyTo, data)
	_serializeEntities(ctx, updateShortMessage.Entities, data)
	return data
}
func UpdateShortSentMessage(ctx *Context, updateShortMessage *tg.UpdateShortSentMessage) JsonData {
	var data = makeJsonData(updateShortMessage)
	_serializeBool(data, updateShortMessage.Out, "out")
	_serializeInt(data, updateShortMessage.ID, "id")
	_serializeInt(data, updateShortMessage.Date, "date")
	_serializeEntities(ctx, updateShortMessage.Entities, data)
	_serializeMedia(ctx, updateShortMessage.Media, data)
	return data
}

func UpdateShortChatMessage(ctx *Context, updateShortMessage *tg.UpdateShortChatMessage) JsonData {
	var data = makeJsonData(updateShortMessage)
	_serializeBool(data, updateShortMessage.Out, "out")
	_serializeBool(data, updateShortMessage.Mentioned, "mentioned")
	_serializeBool(data, updateShortMessage.MediaUnread, "mediaUnread")
	_serializeInt(data, updateShortMessage.ID, "id")
	_serializeInt(data, updateShortMessage.Date, "date")
	_serializeInt64s(data, updateShortMessage.ViaBotID, "viaBotID")
	_serializeInt64s(data, updateShortMessage.FromID, "fromID")
	_serializeInt64s(data, updateShortMessage.ChatID, "chatId")
	_serializeString(data, updateShortMessage.Message, "message")
	_serializeFwdHeader(ctx, &updateShortMessage.FwdFrom, data, "fwdFrom")
	_serializeReplyHeader(ctx, &updateShortMessage.ReplyTo, data, "replyTo")
	_serializeEntities(ctx, updateShortMessage.Entities, data)
	return data
}

func UpdateDifference(ctx *Context, updatesDifferenceClass tg.UpdatesDifferenceClass) JsonData {
	var data = makeJsonData(updatesDifferenceClass)
	// todo
	return data
}

func ChatOnlines(onlines *tg.ChatOnlines) JsonData {
	var data = makeJsonData(onlines)
	_serializeInt(data, onlines.Onlines, "onlines")
	return data
}

func Update(updateClass tg.UpdateClass, ctx *Context) JsonData {
	var serializeData JsonData

	switch update := updateClass.(type) {
	case *tg.UpdateUserStatus:
		serializeData = UserStatus(ctx, update)
	case *tg.UpdateUserName:
		serializeData = UserName(ctx, update)
	case *tg.UpdateUserPhone:
		serializeData = UserPhone(ctx, update)
	case *tg.UpdateNewMessage:
		serializeData = NewMessage(ctx, update)
	case *tg.UpdateNewChannelMessage:
		serializeData = NewChannelMessage(ctx, update)
	case *tg.UpdateEditChannelMessage:
		serializeData = EditChannelMessage(ctx, update)
	case *tg.UpdateDeleteMessages:
		serializeData = DeleteMessages(ctx, update)
	case *tg.UpdateDeleteChannelMessages:
		serializeData = DeleteChannelMessages(ctx, update)
	case *tg.UpdateUserTyping:
		serializeData = UserTyping(ctx, update)
	case *tg.UpdateChatUserTyping:
		serializeData = ChatUserTyping(ctx, update)
	case *tg.UpdateChatParticipants:
		serializeData = UpdateChatParticipants(ctx, update)
	case *tg.UpdateChatParticipant:
		serializeData = UpdateChatParticipant(ctx, update)
	case *tg.UpdateChatParticipantAdd:
		serializeData = UpdateChatParticipantAdd(ctx, update)
	case *tg.UpdateChatParticipantDelete:
		serializeData = UpdateChatParticipantDelete(ctx, update)
	case *tg.UpdateChatParticipantAdmin:
		serializeData = UpdateChatParticipantAdmin(ctx, update)
	case *tg.UpdateChannelMessageViews:
		serializeData = ChannelMessageViews(ctx, update)
	case *tg.UpdateChannelParticipant:
		serializeData = ChannelParticipant(ctx, update)
	case *tg.UpdateGroupCall:
		serializeData = GroupCall(ctx, update)
	case *tg.UpdateGroupCallParticipants:
		serializeData = GroupCallParticipants(ctx, update)
	case *tg.UpdatePhoneCall:
		serializeData = UpdatePhoneCall(ctx, update)
	case *tg.UpdateChannel:
		serializeData = UpdateChannel(ctx, update)
	case *tg.UpdateChat:
		serializeData = UpdateChat(ctx, update)
	case *tg.UpdateServiceNotification:
		serializeData = UpdateServiceNotification(ctx, update)
	case *tg.UpdateMessageReactions:
		fmt.Println("UpdateMessageReactions:", update.Reactions)
	default:
		serializeData = makeJsonData(updateClass)
		UndefinedClass(serializeData, update)
	}

	return serializeData
}

func Updates(updateClass tg.UpdatesClass, ctx *Context) JsonData {
	// todo ???
	var serializeData = makeJsonData(updateClass)
	UndefinedClass(serializeData, updateClass)
	return serializeData
}

func MessagesMessageReactionsList(reactionsList *tg.MessagesMessageReactionsList, ctx *Context) JsonData {
	var serializeData = makeJsonData(reactionsList)
	_serializeInt(serializeData, reactionsList.Count, "count")
	_serializeString(serializeData, reactionsList.NextOffset, "nextOffset")
	var reactions = make([]JsonData, len(reactionsList.Reactions))
	for index, reaction := range reactionsList.Reactions {
		var reactionData = make(JsonData)
		reactions[index] = reactionData
		reactionData["reaction"] = reaction.Reaction
		_serializePeer(ctx, reaction.PeerID, reactionData, "user")
	}
	serializeData["reactions"] = reactions
	return serializeData
}
