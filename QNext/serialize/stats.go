package serialize

import "github.com/gotd/td/tg"

func StatsMegagroupStats(stats *tg.StatsMegagroupStats) JsonData {
	var data = makeJsonData(stats)
	_serializeStatsDateRangeDays(&stats.Period, data)
	_serializeStatsAbsValueAndPrev(&stats.Members, data, "member")
	_serializeStatsAbsValueAndPrev(&stats.Messages, data, "messages")
	_serializeStatsAbsValueAndPrev(&stats.Viewers, data, "viewers")
	_serializeStatsAbsValueAndPrev(&stats.Posters, data, "posters")
	data["growthGraph"] = StatsGraphClass(stats.GrowthGraph)
	data["membersGraph"] = StatsGraphClass(stats.MembersGraph)
	data["newMembersBySourceGraph"] = StatsGraphClass(stats.NewMembersBySourceGraph)
	data["languagesGraph"] = StatsGraphClass(stats.LanguagesGraph)
	data["messagesGraph"] = StatsGraphClass(stats.MessagesGraph)
	data["actionsGraph"] = StatsGraphClass(stats.ActionsGraph)
	data["topHoursGraph"] = StatsGraphClass(stats.TopHoursGraph)
	data["weekdaysGraph"] = StatsGraphClass(stats.WeekdaysGraph)
	data["weekdaysGraph"] = StatsGraphClass(stats.WeekdaysGraph)
	return  data
}

func StatsBroadcastStats(stats *tg.StatsBroadcastStats) JsonData {
	var data = makeJsonData(stats)
	_serializeStatsDateRangeDays(&stats.Period, data)
	_serializeStatsAbsValueAndPrev(&stats.Followers, data, "followers")
	_serializeStatsAbsValueAndPrev(&stats.ViewsPerPost, data, "viewsPerPost")
	_serializeStatsAbsValueAndPrev(&stats.SharesPerPost, data, "sharesPerPost")
	_serializeStatsPercentValue(&stats.EnabledNotifications, data, "enabledNotifications")
	data["growthGraph"] = StatsGraphClass(stats.GrowthGraph)
	data["followersGraph"] = StatsGraphClass(stats.FollowersGraph)
	data["muteGraph"] = StatsGraphClass(stats.MuteGraph)
	data["topHoursGraph"] = StatsGraphClass(stats.TopHoursGraph)
	data["interactionsGraph"] = StatsGraphClass(stats.InteractionsGraph)
	data["ivInteractionsGraph"] = StatsGraphClass(stats.IvInteractionsGraph)
	data["viewsBySourceGraph"] = StatsGraphClass(stats.ViewsBySourceGraph)
	data["newFollowersBySourceGraph"] = StatsGraphClass(stats.NewFollowersBySourceGraph)
	data["languagesGraph"] = StatsGraphClass(stats.LanguagesGraph)
	return  data
}

func _serializeStatsDateRangeDays(stats *tg.StatsDateRangeDays, parentData JsonData) {
	var data = makeJsonData(stats)
	_serializeInt(data, stats.MinDate, "minDate")
	_serializeInt(data, stats.MaxDate, "maxDate")
	parentData["period"] = data
}

func _serializeStatsAbsValueAndPrev(stats *tg.StatsAbsValueAndPrev, parentData JsonData, key string) {
	var data = makeJsonData(stats)
	_serializeFloat64s(data, stats.Current, "current")
	_serializeFloat64s(data, stats.Previous, "previous")
	parentData[key] = data
}

func _serializeStatsPercentValue(stats *tg.StatsPercentValue, parentData JsonData, key string) {
	var data = makeJsonData(stats)
	_serializeFloat64s(data, stats.Part, "part")
	_serializeFloat64s(data, stats.Total, "total")
	parentData[key] = data
}

func StatsGraphClass(statsClass tg.StatsGraphClass) JsonData {
	var data = makeJsonData(statsClass)
	switch status := statsClass.(type) {
	case *tg.StatsGraphAsync:
		_serializeString(data, status.Token, "token")
	case *tg.StatsGraph:
		_serializeJSON(&status.JSON, data, "json")
		_serializeString(data, status.ZoomToken, "zoomToken")
	case *tg.StatsGraphError:
		_serializeString(data, status.Error, "error")
	}
	return data
}

func _serializeJSON(dataJson *tg.DataJSON, parentData JsonData, key string) {
	data := makeJsonData(dataJson)
	_serializeString(data, dataJson.Data, "data")
	parentData[key] = data
}