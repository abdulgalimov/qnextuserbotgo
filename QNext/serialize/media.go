package serialize

import (
	"github.com/gotd/td/tg"
)

func _serializeMedia(ctx *Context, mediaClass tg.MessageMediaClass, parentData JsonData) {
	if mediaClass == nil {
		return
	}
	var data = makeJsonData(mediaClass)
	var title = "media"
	switch media := mediaClass.(type) {
	case *tg.MessageMediaEmpty:
	case *tg.MessageMediaPhoto:
		title = "mediaPhoto"
		_serializePhoto(media.Photo, data)
	case *tg.MessageMediaGeo:
		title = "mediaGeo"
		_serializeGeo(media.Geo, data)
	case *tg.MessageMediaContact:
		title = "mediaContact"
		_serializeMessageMediaContact(media, data)
	case *tg.MessageMediaDocument:
		title = "mediaDocument"
		_serializeDocument(media.Document, data)
	case *tg.MessageMediaVenue:
		title = "mediaVenue"
		_serializeMessageMediaVenue(media, data)
	case *tg.MessageMediaGeoLive:
		title = "mediaGeoLive"
		_serializeMessageMediaGeoLive(media, data)
	case *tg.MessageMediaWebPage:
		title = "mediaWebPage"
		_serializeWebpage(media.Webpage, data)
	case *tg.MessageMediaGame:
		title = "mediaGame"
		_serializeMediaGame(media, data)
	case *tg.MessageMediaInvoice:
		title = "mediaInvoice"
		_serializeMediaInvoice(media, data)
	case *tg.MessageMediaPoll:
		title = "mediaPoll"
		_serializeMediaPoll(ctx, media, data)
	case *tg.MessageMediaDice:
		title = "mediaDice"
		_serializeMediaDice(media, data)
	default:
		UndefinedClass(data, media)
	}
	parentData[title] = data
}

func _serializeMediaPoll(ctx *Context, mediaPoll *tg.MessageMediaPoll, parentData JsonData) {
	parentData["poll"] = _serializePoll(&mediaPoll.Poll)
	parentData["results"] = _serializePollResults(ctx, &mediaPoll.Results)
}

func _serializeMediaDice(mediaDice *tg.MessageMediaDice, parentData JsonData) {
	_serializeInt(parentData, mediaDice.Value, "value")
	_serializeString(parentData, mediaDice.Emoticon, "emoticon")
}

func _serializeMediaInvoice(mediaInvoice *tg.MessageMediaInvoice, parentData JsonData) {
	_serializeBool(parentData, mediaInvoice.ShippingAddressRequested, "shippingAddressRequested")
	_serializeBool(parentData, mediaInvoice.Test, "test")
	_serializeString(parentData, mediaInvoice.Title, "title")
	_serializeString(parentData, mediaInvoice.Description, "description")
	_serializeString(parentData, mediaInvoice.Currency, "currency")
	_serializeString(parentData, mediaInvoice.StartParam, "startParam")
	_serializeInt64s(parentData, mediaInvoice.TotalAmount, "totalAmount")
	_serializeInt(parentData, mediaInvoice.ReceiptMsgID, "receiptMsgID")
}

func _serializeMediaGame(mediaGame *tg.MessageMediaGame, parentData JsonData) {
	_serializeInt64s(parentData, mediaGame.Game.ID, "id")
	_serializeInt64s(parentData, mediaGame.Game.AccessHash, "accessHash")
	_serializeInt64s(parentData, mediaGame.Game.AccessHash, "accessHash")
	_serializeString(parentData, mediaGame.Game.ShortName, "shortName")
	_serializeString(parentData, mediaGame.Game.Title, "title")
	_serializeString(parentData, mediaGame.Game.Description, "description")
	_serializePhoto(mediaGame.Game.Photo, parentData)
	_serializeDocument(mediaGame.Game.Document, parentData)
}

func _serializeMessageMediaGeoLive(mediaGeoLive *tg.MessageMediaGeoLive, parentData JsonData) {
	_serializeGeo(mediaGeoLive.Geo, parentData)
	_serializeInt(parentData, mediaGeoLive.Heading, "heading")
	_serializeInt(parentData, mediaGeoLive.Period, "period")
	_serializeInt(parentData, mediaGeoLive.ProximityNotificationRadius, "proximityNotificationRadius")
}

func _serializeMessageMediaVenue(mediaVenue *tg.MessageMediaVenue, parentData JsonData) {
	_serializeGeo(mediaVenue.Geo, parentData)
	_serializeString(parentData, mediaVenue.Address, "address")
	_serializeString(parentData, mediaVenue.Provider, "provider")
	_serializeString(parentData, mediaVenue.VenueID, "venueId")
	_serializeString(parentData, mediaVenue.VenueType, "venueType")
}

func _serializeMessageMediaContact(mediaContact *tg.MessageMediaContact, parentData JsonData) {
	_serializeString(parentData, mediaContact.PhoneNumber, "phoneNumber")
	_serializeString(parentData, mediaContact.FirstName, "firstName")
	_serializeString(parentData, mediaContact.LastName, "lastName")
	_serializeString(parentData, mediaContact.Vcard, "vcard")
	_serializeInt64(parentData, mediaContact.UserID, "userId")
}

// ******************************************************************************************

func _serializeGeo(geoClass tg.GeoPointClass, parentData JsonData) {
	var data = makeJsonData(geoClass)
	switch geo := geoClass.(type) {
	case *tg.GeoPointEmpty:
	case *tg.GeoPoint:
		data["latitude"] = geo.Lat
		data["longitude"] = geo.Long
		_serializeFloat64s(data, geo.Lat, "latitude")
		_serializeFloat64s(data, geo.Long, "longitude")
		_serializeInt64s(data, geo.AccessHash, "accessHash")
		_serializeInt(data, geo.AccuracyRadius, "accuracyRadius")
	default:
		UndefinedClass(data, geo)
	}
	parentData["geo"] = data
}

func _serializeDocument(documentClass tg.DocumentClass, parentData JsonData) {
	if documentClass == nil {
		return
	}
	var data = makeJsonData(documentClass)
	switch document := documentClass.(type) {
	case *tg.Document:
		_serializeInt64s(data, document.ID, "id")
		_serializeInt64s(data, document.AccessHash, "accessHash")
		_serializeInt(data, document.Date, "date")
		_serializeInt(data, document.Size, "size")
		_serializeString(data, document.MimeType, "mimeType")
		_serializeStringBytes(data, document.FileReference, "fileReference")
	default:
		UndefinedClass(data, document)
	}
	parentData["document"] = data
}

func _serializePhoto(photoClass tg.PhotoClass, parentData JsonData) {
	if photoClass == nil {
		return
	}
	var data = makeJsonData(photoClass)
	switch photo := photoClass.(type) {
	case *tg.Photo:
		_serializeInt(data, photo.Date, "date")
		_serializeIdInt64(data, photo.ID)
		_serializeInt64s(data, photo.AccessHash, "accessHash")
		_serializeBool(data, photo.HasStickers, "hasStickers")
		_serializeStringBytes(data, photo.FileReference, "fileReference")
		break
	case *tg.PhotoEmpty:
		_serializeIdInt64(data, photo.ID)
		break
	default:
		UndefinedClass(data, photo)
	}
	parentData["photo"] = data
}

func _serializeProfilePhoto(profilePhotoClass tg.UserProfilePhotoClass, parentData JsonData) {
	if profilePhotoClass == nil {
		return
	}
	var data = makeJsonData(profilePhotoClass)
	switch profilePhoto := profilePhotoClass.(type) {
	case *tg.UserProfilePhotoEmpty:
		break
	case *tg.UserProfilePhoto:
		_serializeIdInt64(data, profilePhoto.PhotoID)
		_serializeBool(data, profilePhoto.HasVideo, "hasVideo")
		break
	default:
		UndefinedClass(data, profilePhoto)
	}
	parentData["photo"] = data
}
