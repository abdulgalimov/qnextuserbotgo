package serialize

import "github.com/gotd/td/tg"

func _serializeWebpage(webpageClass tg.WebPageClass, parentData JsonData) {
	var data = makeJsonData(webpageClass)
	switch webpage := webpageClass.(type) {
	case *tg.WebPageEmpty:
		_serializeInt64s(data, webpage.ID, "id")
	case *tg.WebPagePending:
		_serializeInt64s(data, webpage.ID, "id")
		_serializeInt(data, webpage.Date, "date")
	case *tg.WebPageNotModified:
		_serializeInt(data, webpage.CachedPageViews, "cachedPageViews")
	case *tg.WebPage:
		_serializeInt64s(data, webpage.ID, "id")
		_serializeString(data, webpage.URL, "url")
		_serializeString(data, webpage.DisplayURL, "displayUrl")
		_serializeString(data, webpage.Type, "type")
		_serializeString(data, webpage.SiteName, "siteName")
		_serializeString(data, webpage.Title, "title")
		_serializeString(data, webpage.Description, "description")
		_serializeString(data, webpage.EmbedURL, "embedURL")
		_serializeString(data, webpage.EmbedType, "embedType")
		_serializeString(data, webpage.Author, "author")
		_serializeInt(data, webpage.Hash, "hash")
		_serializeInt(data, webpage.EmbedWidth, "embedWidth")
		_serializeInt(data, webpage.EmbedHeight, "embedHeight")
		_serializeInt(data, webpage.Duration, "duration")
		// todo other properties
	default:
		UndefinedClass(data, webpage)

	}
	parentData["webpage"] = data
}
