package QNext

import (
	"context"
	"fmt"
	"github.com/gotd/td/session"
	"github.com/gotd/td/telegram"
	"github.com/gotd/td/telegram/auth"
	"github.com/gotd/td/tg"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/xerrors"
	"os"
	"os/signal"
	"qnextUserBot/Common"
	"time"
)

type AuthStatus string

const (
	ErrorAuthStatus        AuthStatus = "ErrorAuthStatus"
	NotFoundStatus         AuthStatus = "NotFoundStatus"
	NotStartedStatus       AuthStatus = "NotStartedStatus"
	CreateStatus           AuthStatus = "CreateStatus"
	NeedPhoneStatus        AuthStatus = "NeedPhoneStatus"
	ReceivedPhoneStatus    AuthStatus = "ReceivedPhoneStatus"
	NeedAuthCodeStatus     AuthStatus = "NeedAuthCodeStatus"
	ReceivedAuthCodeStatus AuthStatus = "ReceivedAuthCodeStatus"
	NeedPasswordStatus     AuthStatus = "NeedPasswordStatus"
	ReceivedPasswordStatus AuthStatus = "ReceivedPasswordStatus"
	NeedAcceptTerms        AuthStatus = "NeedAcceptTermsStatus"
	AcceptedTerms          AuthStatus = "AcceptedTerms"
	ReadyStatus            AuthStatus = "ReadyStatus"
	ClosedStatus           AuthStatus = "ClosedStatus"
)

func getSessionFile(userBotId int) string {
	return fmt.Sprintf("%s/database/%d.json", Common.Config.App.DatabaseDir, userBotId)
}

var userBots = make(map[int]*UserBot)

func Create(botId int, userBotId int, apiId int, apiHash string) *Common.Result {
	if userBots[userBotId] != nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.CreateDuplicate,
		}
	}
	fmt.Println("QNext:Create", userBotId, apiId, apiHash)

	var updateHandler = &UpdateHandler{
		UserBotId: userBotId,
		BotId:     botId,
	}
	var options, _ = telegram.OptionsFromEnvironment(telegram.Options{
		UpdateHandler: updateHandler,
		SessionStorage: &session.FileStorage{
			Path: getSessionFile(userBotId),
		},
	})
	if Common.Config.UserBot.Logger {
		logger, _ := zap.NewDevelopment(zap.IncreaseLevel(zapcore.DebugLevel))
		defer func() { _ = logger.Sync() }()
		options.Logger = logger
	}

	valueCtx := context.WithValue(context.Background(), "userBotId", userBotId)
	ctx, cancel := signal.NotifyContext(valueCtx, os.Interrupt)
	client := telegram.NewClient(apiId, apiHash, options)
	updateHandler.Client = client

	var userBot = &UserBot{
		id:         userBotId,
		client:     client,
		Context:    ctx,
		cancel:     cancel,
		status:     CreateStatus,
		readValues: CreateReadValue(userBotId, "values"),
	}
	userBots[userBotId] = userBot

	var onRun = func(ctx context.Context) error {
		err := client.Auth().IfNecessary(ctx, auth.NewFlow(
			userBot,
			auth.SendCodeOptions{},
		))
		if err != nil {
			return xerrors.Errorf("auth: %w", err)
		}
		userBot.SetAuthStatus(ReadyStatus)

		raw := tg.NewClient(client)
		userBot.Raw = raw

		cfg, err := raw.HelpGetConfig(ctx)
		if err != nil {
			return err
		}
		userBot.cfg = cfg

		appConfig, err := raw.HelpGetAppConfig(ctx)
		userBot.appConfig = appConfig

		<-ctx.Done()
		return ctx.Err()
	}

	go func() {
		err := client.Run(ctx, onRun)
		if err != nil {
			fmt.Println("Error client stop ", err)
			userBot.SetAuthStatus(ErrorAuthStatus)
		}
	}()

	return &Common.Result{Ok: true}
}

func PingUp(botId int, userBotId int, apiId int, apiHash string) *Common.Result {
	fmt.Println("QNext:PingUp", userBotId)

	if userBots[userBotId] == nil {
		sessionFile := getSessionFile(userBotId)
		if _, err := os.Stat(sessionFile); os.IsNotExist(err) {
			return &Common.Result{
				Ok:           false,
				ErrorMessage: Common.BotNotFound,
			}
		}
		Create(botId, userBotId, apiId, apiHash)
	}

	return &Common.Result{Ok: true}
}

func Restart(botId int, userBotId int, apiId int, apiHash string) *Common.Result {
	if userBots[userBotId] != nil {
		Stop(userBotId)
	}
	var sessionFile = getSessionFile(userBotId)
	err := os.Remove(sessionFile)
	if err != nil {
		fmt.Println("remove session file", err)
	}
	return Create(botId, userBotId, apiId, apiHash)
}

func SetPhone(userBotId int, phoneNumber string) *Common.Result {
	var userBot = userBots[userBotId]
	if userBot == nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotNotFound,
		}
	}
	if userBot.status != NeedPhoneStatus {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotInvalidStatus,
		}
	}
	fmt.Println("QNext:SetPhone", userBotId, phoneNumber)
	userBot.readValues.setValue(phoneNumber)

	return &Common.Result{Ok: true}
}

func SetCode(userBotId int, authCode string) *Common.Result {
	var userBot = userBots[userBotId]
	if userBot == nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotNotFound,
		}
	}
	if userBot.status != NeedAuthCodeStatus {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotInvalidStatus,
		}
	}
	fmt.Println("QNext:SetCode", userBotId, authCode)
	userBot.readValues.setValue(authCode)

	return &Common.Result{Ok: true}
}

func SetPassword(userBotId int, password string) *Common.Result {
	var userBot = userBots[userBotId]
	if userBot == nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotNotFound,
		}
	}
	if userBot.status != NeedPasswordStatus {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotInvalidStatus,
		}
	}
	userBot.readValues.setValue(password)

	return &Common.Result{Ok: true}
}

func GetStatus(userBotId int) (*Common.Result, string) {
	var userBot = userBots[userBotId]
	if userBot == nil {
		var sessionFile = getSessionFile(userBotId)
		var status AuthStatus
		if _, err := os.Stat(sessionFile); !os.IsNotExist(err) {
			status = NotStartedStatus
		} else {
			status = NotFoundStatus
		}
		var statusStr = string(status)
		return &Common.Result{Ok: true}, statusStr
	}
	fmt.Println("QNext:GetStatus", userBotId)

	if userBot.status == ReadyStatus {
		timeoutCtx, cancelFunc := context.WithTimeout(context.Background(), time.Second*1000)
		defer cancelFunc()

		status, err := userBot.client.Auth().Status(timeoutCtx)
		if err != nil {
			userBot.SetAuthStatus(ErrorAuthStatus)
		} else if !status.Authorized {
			userBot.SetAuthStatus(ErrorAuthStatus)
		}
	}

	fmt.Println("QNext:GetStatus complete")

	var statusStr = string(userBot.status)
	return &Common.Result{Ok: true}, statusStr
}

func Delete(userBotId int) *Common.Result {
	fmt.Println("QNext:Delete", userBotId)

	Stop(userBotId)

	sessionFile := getSessionFile(userBotId)
	err := os.RemoveAll(sessionFile)
	if err != nil {
		fmt.Println("Remove err", err)
	}

	return &Common.Result{
		Ok: true,
	}
}

func Stop(userBotId int) *Common.Result {
	if userBots[userBotId] == nil {
		return &Common.Result{
			Ok:           false,
			ErrorMessage: Common.BotNotFound,
		}
	}

	fmt.Println("QNext:Stop", userBotId)
	userBots[userBotId].cancel()
	delete(userBots, userBotId)

	return &Common.Result{
		Ok: true,
	}
}
