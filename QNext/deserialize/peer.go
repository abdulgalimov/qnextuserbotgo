package deserialize

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func GetInputPeer(data Common.DataReader, key string) (tg.InputPeerClass, error) {
	peerData, errData := data.GetReader(key)
	if errData != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: key,
		}
	}

	constructor, errConstructor := peerData.GetString("constructor")
	if errConstructor != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "constructor",
		}
	}

	switch constructor {
	case "inputPeerEmpty":
		return &tg.InputPeerEmpty{}, nil
	case "inputPeerSelf":
		return &tg.InputPeerSelf{}, nil
	case "inputPeerChat":
		return GetInputPeerChat(peerData)
	case "inputPeerChannel":
		return GetInputPeerChannel(peerData)
	case "inputPeerUser":
		return GetInputPeerUser(peerData)
	case "inputPeerUserFromMessage":
		return GetInputPeerUserFromMessage(peerData)
	case "inputPeerChannelFromMessage":
		return GetInputPeerChannelFromMessage(peerData)
	}

	return nil, &Common.ParameterTypeInvalidError{
		Parameter: "constructor",
	}
}

func GetInputPeerChat(data Common.DataReader) (*tg.InputPeerChat, error) {
	chatId, errChatId := data.GetChatId("chatId")
	if errChatId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "chatId",
		}
	}
	return &tg.InputPeerChat{
		ChatID: chatId,
	}, nil
}

func GetInputPeerChannel(data Common.DataReader) (*tg.InputPeerChannel, error) {
	channelId, errChatId := data.GetChannelId("channelId")
	if errChatId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "channelId",
		}
	}
	accessHash, errAccessHash := data.GetInt64("accessHash")
	if errAccessHash != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "accessHash",
		}
	}
	return &tg.InputPeerChannel{
		ChannelID:  channelId,
		AccessHash: accessHash,
	}, nil
}

func GetInputPeerUser(data Common.DataReader) (*tg.InputPeerUser, error) {
	userId, errChatId := data.GetInt64("userId")
	if errChatId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "userId",
		}
	}
	accessHash, errAccessHash := data.GetInt64("accessHash")
	if errAccessHash != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "accessHash",
		}
	}
	return &tg.InputPeerUser{
		UserID:     userId,
		AccessHash: accessHash,
	}, nil
}

func GetInputPeerUserFromMessage(data Common.DataReader) (*tg.InputPeerUserFromMessage, error) {
	peer, errPeer := GetInputPeer(data, "peer")
	if errPeer != nil {
		return nil, &Common.ParameterTypeInvalidError{
			Parameter: "peer",
		}
	}

	messageId, errMessageId := data.GetInt("messageId")
	if errMessageId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "messageId",
		}
	}

	userId, errChatId := data.GetInt64("userId")
	if errChatId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "userId",
		}
	}

	return &tg.InputPeerUserFromMessage{
		Peer:   peer,
		MsgID:  messageId,
		UserID: userId,
	}, nil
}

func GetInputPeerChannelFromMessage(data Common.DataReader) (*tg.InputPeerChannelFromMessage, error) {
	peer, errPeer := GetInputPeer(data, "peer")
	if errPeer != nil {
		return nil, &Common.ParameterTypeInvalidError{
			Parameter: "peer",
		}
	}

	messageId, errMessageId := data.GetInt("messageId")
	if errMessageId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "messageId",
		}
	}

	channelId, errChannelID := data.GetChannelId("channelId")
	if errChannelID != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "channelId",
		}
	}

	return &tg.InputPeerChannelFromMessage{
		Peer:      peer,
		MsgID:     messageId,
		ChannelID: channelId,
	}, nil
}
