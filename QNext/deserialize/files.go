package deserialize

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func GetInputFileLocation(data Common.DataReader) (tg.InputFileLocationClass, error) {
	fileData, errData := data.GetReader("location")
	if errData != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "location",
		}
	}

	constructor, errConstructor := fileData.GetString("constructor")
	if errConstructor != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "constructor",
		}
	}

	switch constructor {
	case "inputPhotoFileLocation":
		return GetInputPhotoFileLocation(fileData)
	case "inputStickerSetThumb":
		return GetInputStickerSetThumb(fileData)
	}

	return nil, &Common.ParameterTypeInvalidError{
		Parameter: "constructor",
	}
}

func GetInputPhotoFileLocation(data Common.DataReader) (*tg.InputPhotoFileLocation, error) {
	id, errId := data.GetInt64("id")
	if errId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "id",
		}
	}
	accessHash, errAccessHash := data.GetInt64("accessHash")
	if errAccessHash != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "accessHash",
		}
	}

	fileReference, errFileReference := data.GetFileReference("fileReference")
	if errFileReference != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "fileReference",
		}
	}

	thumbSize, errThumbSize := data.GetString("thumbSize")
	if errThumbSize != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "thumbSize",
		}
	}

	return &tg.InputPhotoFileLocation{
		ID: id,
		AccessHash: accessHash,
		FileReference: fileReference,
		ThumbSize: thumbSize,
	}, nil
}


func GetInputStickerSetThumb(data Common.DataReader) (*tg.InputStickerSetThumb, error) {
	stickerSet, err := GetInputStickerSet(data, "stickerSet")
	if err != nil {
		return nil, err
	}
	thumbVersion, errVersion := data.GetInt("thumbVersion")
	if errVersion != nil {
		return nil, errVersion
	}

	return &tg.InputStickerSetThumb{
		Stickerset: stickerSet,
		ThumbVersion: thumbVersion,
	}, nil
}