package deserialize

import (
	"qnextUserBot/Common"
	"strconv"
)

func _deserializeInt64s(data Common.DataReader, key string) (int64, error) {
	valueStr, err1 := data.GetString(key)
	if err1 != nil {
		return 0, err1
	}

	value, err2 := strconv.ParseInt(valueStr, 10, 64)
	if err2 != nil {
		return 0, err2
	}
	return value, nil
}
