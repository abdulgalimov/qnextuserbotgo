package deserialize

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func GetMessagesGetStickerSetRequest(data Common.DataReader) (*tg.MessagesGetStickerSetRequest, error) {
	inputSticker, err := GetInputStickerSet(data, "sticker")
	if err != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "sticker",
		}
	}
	return &tg.MessagesGetStickerSetRequest{
		Stickerset: inputSticker,
	}, nil
}

func GetInputStickerSet(data Common.DataReader, key string) (tg.InputStickerSetClass, error) {
	stickerData, errData := data.GetReader(key)
	if errData != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "sticker",
		}
	}

	constructor, errConstructor := stickerData.GetString("constructor")
	if errConstructor != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "constructor",
		}
	}

	switch constructor {
	case "inputStickerSetShortName":
		return GetInputStickerSetShortName(stickerData)
	case "inputStickerSetID":
		return GetInputStickerSetID(stickerData)
	}

	return nil, &Common.ParameterTypeInvalidError{
		Parameter: "constructor",
	}
}

func GetInputStickerSetShortName(data Common.DataReader) (*tg.InputStickerSetShortName, error) {
	shortName, err := data.GetString("shortName")
	if err != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "shortName",
		}
	}
	return &tg.InputStickerSetShortName{
		ShortName: shortName,
	}, nil
}

func GetInputStickerSetID(data Common.DataReader) (*tg.InputStickerSetID, error) {
	id, errId := data.GetInt64("id")
	if errId != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "id",
		}
	}
	accessHash, errAccessHash := data.GetInt64("accessHash")
	if errAccessHash != nil {
		return nil, &Common.ParameterNotFoundError{
			Parameter: "accessHash",
		}
	}
	return &tg.InputStickerSetID{
		ID:         id,
		AccessHash: accessHash,
	}, nil
}
