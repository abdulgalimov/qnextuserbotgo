package deserialize

import (
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func GetInputChannelClass(data Common.DataReader) (tg.InputChannelClass, error)  {
	channelData, errData := data.GetReader("channel")
	if errData != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "channel",
		}
	}

	constructor, errConstructor := channelData.GetString("constructor")
	if errConstructor != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "constructor",
		}
	}

	switch constructor {
	case "inputChannel":
		return GetInputChannel(channelData)
	case "inputChannelFromMessage":
		return GetInputChannelFromMessage(channelData)
	}

	return nil, nil
}

func GetInputChannel(data Common.DataReader) (*tg.InputChannel, error) {
	channelId64, chatIdErr := data.GetChannelId("channelId")
	if chatIdErr != nil {
		return nil, chatIdErr
	}
	//
	accessHash, accessHashErr := _deserializeInt64s(data, "accessHash")
	if accessHashErr != nil {
		return nil, accessHashErr
	}
	return &tg.InputChannel{
		ChannelID:  channelId64,
		AccessHash: accessHash,
	}, nil
}

func GetInputChannelFromMessage(data Common.DataReader) (*tg.InputChannelFromMessage, error) {
	peer, errPeer := GetInputPeer(data, "peer")
	if errPeer != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "peer",
		}
	}

	channelId64, chatIdErr := data.GetChannelId("channelId")
	if chatIdErr != nil {
		return nil, chatIdErr
	}
	//
	messageId, errMessageId := data.GetInt("messageId")
	if errMessageId != nil {
		return nil, errMessageId
	}
	return &tg.InputChannelFromMessage{
		Peer: peer,
		ChannelID:  channelId64,
		MsgID: messageId,
	}, nil
}