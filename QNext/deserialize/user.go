package deserialize

import (
	"errors"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
)

func GetInputUserClass(data Common.DataReader) (tg.InputUserClass, error) {
	userData, errData := data.GetReader("user")
	if errData != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "user",
		}
	}

	constructor, errConstructor := userData.GetString("constructor")
	if errConstructor != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "constructor",
		}
	}

	switch constructor {
	case "inputUserSelf":
		return &tg.InputUserSelf{}, nil
	case "inputUser":
		return GetInputUser(userData)
	case "inputUserFromMessage":
		return GetInputUserFromMessage(userData)
	}

	return nil, errors.New("invalid targetId")
}

func GetInputUser(data Common.DataReader) (*tg.InputUser, error) {
	userId, errUserId := data.GetInt64("userId")
	if errUserId != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "userId",
		}
	}
	accessHash, errAccessHash := data.GetInt64("accessHash")
	if errAccessHash != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "errAccessHash",
		}
	}

	return &tg.InputUser{
		UserID:     userId,
		AccessHash: accessHash,
	}, nil
}

func GetInputUserFromMessage(data Common.DataReader) (*tg.InputUserFromMessage, error) {
	inputPeer, errPeer := GetInputPeer(data, "peer")
	if errPeer != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "peer",
		}
	}

	userId, errUserId := data.GetInt64("userId")
	if errUserId != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "userId",
		}
	}
	messageId, errMessageId := data.GetInt("messageId")
	if errMessageId != nil {
		return nil, Common.ParameterTypeInvalidError{
			Parameter: "messageId",
		}
	}

	return &tg.InputUserFromMessage{
		Peer:   inputPeer,
		UserID: userId,
		MsgID:  messageId,
	}, nil
}
