package QNext

import (
	"context"
	"github.com/gotd/td/telegram"
	"github.com/gotd/td/tg"
	"qnextUserBot/Common"
	"qnextUserBot/QNext/serialize"
)

type UpdateHandler struct {
	Client      *telegram.Client
	tgClient    *tg.Client
	UserBotId   int
	BotId       int
	UpdateFlags UpdateFlags
}

func (h *UpdateHandler) Handle(ctx context.Context, u tg.UpdatesClass) error {
	//fmt.Println("Handle", u)
	switch update := u.(type) {
	case *tg.UpdatesTooLong:
		return h.handleTooLong(ctx, update)
	case *tg.UpdateShortMessage:
		return h.handleShortMessage(update)
	case *tg.UpdateShort:
		return h.handleShort(update)
	case *tg.UpdatesCombined:
		return h.handleCombined(update)
	case *tg.Updates:
		return h.handleUpdates(update)
	case *tg.UpdateShortSentMessage:
		return h.handleShortSentMessage(update)
	case *tg.UpdateShortChatMessage:
		return h.updateShortChatMessage(update)
	}
	return nil
}
func (h *UpdateHandler) handleTooLong(ctx context.Context, updates *tg.UpdatesTooLong) error {
	req := &tg.UpdatesGetDifferenceRequest{}
	if h.tgClient == nil {
		h.tgClient = tg.NewClient(h.Client)
	}
	updatesDifference, err := h.tgClient.UpdatesGetDifference(ctx, req)
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(0, nil, nil)
	if err == nil {
		serializeCtx.Data = serialize.UpdateDifference(&serializeCtx, updatesDifference)
	} else {
		var errorData = make(serialize.JsonData)
		errorData["error"] = err.Error()
		serializeCtx.Data = errorData
	}
	sendToApp(h, updatesDifference, &serializeCtx)
	return nil
}
func (h *UpdateHandler) handleShortMessage(updateShort *tg.UpdateShortMessage) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(updateShort.Date, nil, nil)
	serializeCtx.Data = serialize.UpdateShortMessage(&serializeCtx, updateShort)
	sendToApp(h, updateShort, &serializeCtx)
	return nil
}
func (h *UpdateHandler) handleShort(update *tg.UpdateShort) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(update.Date, nil, nil)
	serializeCtx.Data = serialize.Update(update.Update, &serializeCtx)
	sendToApp(h, update, &serializeCtx)
	return nil
}
func (h *UpdateHandler) handleCombined(updateCombined *tg.UpdatesCombined) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(updateCombined.Date, updateCombined.Users, updateCombined.Chats)
	for _, update := range updateCombined.Updates {
		serializeCtx.Data = serialize.Update(update, &serializeCtx)
		sendToApp(h, updateCombined, &serializeCtx)
	}
	serializeCtx.Clear()
	return nil
}
func (h *UpdateHandler) handleUpdates(updates *tg.Updates) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(updates.Date, updates.Users, updates.Chats)
	for _, update := range updates.Updates {
		serializeCtx.Data = serialize.Update(update, &serializeCtx)
		sendToApp(h, updates, &serializeCtx)
	}
	serializeCtx.Clear()
	return nil
}
func (h *UpdateHandler) handleShortSentMessage(updateShortSent *tg.UpdateShortSentMessage) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(updateShortSent.Date, nil, nil)
	serializeCtx.Data = serialize.UpdateShortSentMessage(&serializeCtx, updateShortSent)
	sendToApp(h, updateShortSent, &serializeCtx)
	return nil
}

func (h *UpdateHandler) updateShortChatMessage(updateShortChat *tg.UpdateShortChatMessage) error {
	var serializeCtx = serialize.Context{}
	serializeCtx.Init(updateShortChat.Date, nil, nil)
	serializeCtx.Data = serialize.UpdateShortChatMessage(&serializeCtx, updateShortChat)
	sendToApp(h, updateShortChat, &serializeCtx)
	return nil
}

func sendToApp(updateHandler *UpdateHandler, updateClass interface{}, serializeCtx *serialize.Context) {
	/*
		if !checkFlags(updateHandler, updateClass) {
			return
		}
	*/
	//
	if serializeCtx.Data != nil {
		serializeCtx.Data["date"] = serializeCtx.Date
	}

	var nativeData = make(serialize.JsonData)
	nativeData["Update"] = updateClass
	nativeData["Chats"] = serializeCtx.Chats
	nativeData["Users"] = serializeCtx.Users

	var rawEvent = Common.RawEvent{
		BotId:      updateHandler.BotId,
		UserBotId:  updateHandler.UserBotId,
		RawData:    serializeCtx.Data,
		NativeData: nativeData,
	}
	Common.GlobalRawEvents <- &rawEvent
}

func checkFlags(updateHandler *UpdateHandler, updateClass tg.UpdateClass) bool {
	switch updateClass.(type) {
	case *tg.UpdateUserStatus:
		return updateHandler.UpdateFlags.HasFlag1(UpdateUserStatusFlag1)
	case *tg.UpdateUserName:
		return updateHandler.UpdateFlags.HasFlag1(UpdateUserNameFlag1)
	case *tg.UpdateUserPhone:
		return updateHandler.UpdateFlags.HasFlag1(UpdateUserPhoneFlag1)
	case *tg.UpdateNewMessage:
		return updateHandler.UpdateFlags.HasFlag1(UpdateNewMessageFlag1)
	case *tg.UpdateNewChannelMessage:
		return updateHandler.UpdateFlags.HasFlag1(UpdateNewChannelMessageFlag1)
	case *tg.UpdateDeleteMessages:
		return updateHandler.UpdateFlags.HasFlag1(UpdateDeleteMessagesFlag1)
	case *tg.UpdateDeleteChannelMessages:
		return updateHandler.UpdateFlags.HasFlag1(UpdateDeleteChannelMessagesFlag1)
	case *tg.UpdateUserTyping:
		return updateHandler.UpdateFlags.HasFlag1(UpdateUserTypingFlag1)
	case *tg.UpdateChatUserTyping:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatUserTypingFlag1)
	case *tg.UpdateChatParticipants:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatParticipantsFlag1)
	case *tg.UpdateChatParticipant:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatParticipantFlag1)
	case *tg.UpdateChatParticipantAdd:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatParticipantAddFlag1)
	case *tg.UpdateChatParticipantDelete:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatParticipantDeleteFlag1)
	case *tg.UpdateChatParticipantAdmin:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatParticipantAdminFlag1)
	case *tg.UpdateChannelMessageViews:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChannelMessageViewsFlag1)
	case *tg.UpdateChannelParticipant:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChannelParticipantFlag1)
	case *tg.UpdateGroupCall:
		return updateHandler.UpdateFlags.HasFlag1(UpdateGroupCallFlag1)
	case *tg.UpdateGroupCallParticipants:
		return updateHandler.UpdateFlags.HasFlag1(UpdateGroupCallParticipantsFlag1)
	case *tg.UpdatePhoneCall:
		return updateHandler.UpdateFlags.HasFlag1(UpdatePhoneCallFlag1)
	case *tg.UpdateChannel:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChannelFlag1)
	case *tg.UpdateChat:
		return updateHandler.UpdateFlags.HasFlag1(UpdateChatFlag1)
	default:
		return updateHandler.UpdateFlags.HasFlag1(UpdateUndefinedFlag1)
	}
}
